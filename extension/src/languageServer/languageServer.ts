import * as os from "os";
import * as path from "path";
import * as vscode from "vscode";
import { commands, Uri, window, workspace } from "vscode";
import { Trace } from "vscode-jsonrpc";
import {
  LanguageClient,
  LanguageClientOptions,
  ServerOptions
} from "vscode-languageclient";
var net = require('net')
export class LanguageServer extends vscode.Disposable {

  private _disposables: vscode.Disposable[] = [];

  constructor(context) {
    super(() => this.dispose());
    this.register(context);
  }
  dispose() {
    this._disposables && this._disposables.length && this._disposables.map(d => d.dispose());
  }
  register(context) {
    // The server is a locally installed in src/pf
    let launcher =
      os.platform() === "win32" ? "pf-standalone.bat" : "pf-standalone";
    let script = context.asAbsolutePath(path.join("extension", "src", "pf", "bin", launcher));

    let serverOptions: ServerOptions = {
      run: { command: script },
      debug: { command: script, args: [], options: { env: createDebugEnv() } },
    };

    let clientOptions: LanguageClientOptions = {
      documentSelector: ["pf"],
      synchronize: {
        fileEvents: workspace.createFileSystemWatcher("**/*.*"),
      },
    };

    // Create the language client and start the client.
    let lc = new LanguageClient("Xtext Server", serverOptions, clientOptions);

    var disposable2 = commands.registerCommand("pf.a.proxy", async () => {
      let activeEditor = window.activeTextEditor;
      if (
        !activeEditor ||
        !activeEditor.document ||
        activeEditor.document.languageId !== "pf"
      ) {
        return;
      }

      if (activeEditor.document.uri instanceof Uri) {
        commands.executeCommand("pf.a", activeEditor.document.uri.toString());
      }
    });
    this._disposables.push(disposable2);

    // enable tracing (.Off, .Messages, Verbose)
    lc.trace = Trace.Verbose;
    let disposable = lc.start();

    // Push the disposable to the context's subscriptions so that the
    // client can be deactivated on extension deactivation
    this._disposables.push(disposable);
  }

}

function createDebugEnv() {
  return Object.assign(
    {
      JAVA_OPTS:
        "-Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n,quiet=y",
    },
    process.env
  );
}

