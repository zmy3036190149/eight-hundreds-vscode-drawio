import * as fs from "fs";
import * as path from "path";
import * as vscode from "vscode";
import { Logger } from "./logger";
import { GetProject } from "./xml/GetProject";
import { SaveProject } from "./xml/SaveProject";
var XMLWriter = require('xml-writer')
const viewType = "vscode-drawio.editor";
export class DrawIOEditorProvider implements vscode.CustomTextEditorProvider {
  static register(context: vscode.ExtensionContext) {
    const provider = new DrawIOEditorProvider(context);
    const providerRegistration = vscode.window.registerCustomEditorProvider(viewType, provider);
    return providerRegistration;
  }
  savePro = new SaveProject();
  getPro = new GetProject();
  constructor(private readonly context: vscode.ExtensionContext) { }

  resolveCustomTextEditor(
    document: vscode.TextDocument,
    webviewPanel: vscode.WebviewPanel,
    token: vscode.CancellationToken
  ): void | Thenable<void> {
    const drawIoAppRoot = path.join(this.context.extensionPath, "drawioApp");
    let indexContent = fs.readFileSync(path.join(drawIoAppRoot, "index.html"), "utf8");
    indexContent = indexContent.replace(
      /#vscode-root#/g,
      webviewPanel.webview.asWebviewUri(vscode.Uri.file(drawIoAppRoot)).toString()
    );
    indexContent = indexContent.replace(
      /#init-localStorage#/,
      `vscode.setState(${this.context.workspaceState.get("drawio") || "{}"})`
    );
    indexContent = indexContent.replace(/#init-localStorage#/, "");

    webviewPanel.webview.html = indexContent;
    webviewPanel.webview.options = {
      enableScripts: true,
    };

    //插件接受消息
    webviewPanel.webview.onDidReceiveMessage((e) => {
      if (e.event == "autosaveProject") {
        // console.log("++++++++++++++e++++++++++++++")
        // console.log(e.data)
        this.updateProjectDocument(document, e.data, webviewPanel)
      }
      if (e.type === "error") {
        return;
      }
      const data = e.data;
      Logger.debug(`message from drawio:${JSON.stringify(data)}`);
      if (e.type === "data") {
        this.dataEventHandler(data, document, webviewPanel);
      }
      if (e.type === "setting") {
        this.settingEventHandler(data, document, webviewPanel);
      }
    });

  }

  dataEventHandler(data: any, document: vscode.TextDocument, webviewPanel: vscode.WebviewPanel) {
    if (data.event === "init") {
      //插件给webview发送消息
      // console.log("webview给插件发送消息init,插件给webview发送消息load,若没有load消息，webview将无法加载")
      var xml = '<mxfile host="6d06fdce-f48c-4e47-924a-8199e10ced45" modified="2020-09-15T05:52:00.609Z" agent="Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Code/1.47.1 Chrome/78.0.3904.130 Electron/7.3.2 Safari/537.36" etag="WES1PhOKQFfZLKD-5O76" version="@DRAWIO-VERSION@" pages="2"><diagram id="vknM0KYCZObk2hPpuepO" name="ProblemDiagram">ddG9DoMgEADgp2FXSKydrW0XJ4fORK5Cgp5BGm2fvprTWmK7kOO7g+OHiawZL052ukAFlvFIjUycGOc8TaZxhieBiFKC2hlFFG9QmhcsGC36MAr6oNAjWm+6ECtsW6h8YNI5HMKyO9qwaydr2EFZSbvXm1Fek6b8sPkVTK3XznFypEwj1+LlJr2WCocvEjkTmUP0FDVjBnZ+u/VdaN35T/ZzMAet/7FgCra9p0nwQSJ/Aw==</diagram></mxfile>'
      webviewPanel.webview.postMessage({
        action: "load",
        autosave: 1,
        xml: xml,
      });
      this.loadProject(document, webviewPanel)

    }

    if (data.event === "save") {
      // console.log("save")
      // webviewPanel.webview.postMessage({
      //   action: "export",
      //   xml: data.xml,
      // });
    }
    if (data.event === "autosave") {
      // console.log("autosave")
      //this.updateProjectDocument(document, data.xml, webviewPanel)
      //this.updateDocument(document, data.xml);
    }

    if (data.event === "export") {
      //console.log("export")
      this.updateDocument(document, data.xml, true);
    }
    if (data.event === "error") {
      Logger.debug(`Error: ${JSON.stringify(data)}`);
    }
  }

  settingEventHandler(data: any, document: vscode.TextDocument, webviewPanel: vscode.WebviewPanel) {
    this.context.workspaceState.update("drawio", JSON.stringify(data || {}));
  }
  private loadProject(document: vscode.TextDocument, webviewPanel: vscode.WebviewPanel) {
    //从文件获取project
    var oldFsPath = document.uri.fsPath
    // var proPath = oldFsPath.replace(/(.*)\\(.*)/, "$1\\Project.xml")

    this.getPro.getProject(oldFsPath).then((data) => {
      webviewPanel.webview.postMessage({
        action: "loadProject",
        autosave: 1,
        title: document.fileName.replace(".project.xml", ""),
        project: data
      });
    }).catch((error) => {
      console.log(error)
    })

  }
  private updateProjectDocument(textDocument: vscode.TextDocument, project, webviewPanel: vscode.WebviewPanel, save?: boolean) {
    var oldFsPath = textDocument.uri.fsPath
    var proPath = oldFsPath.replace(/(.*)\\(.*)/, "$1\\Project.xml")
    this.savePro.saveProject(proPath, project);
    var cdPath = oldFsPath.replace(/(.*)\\(.*)/, "$1\\contextDiagram.xml")
    this.savePro.saveContextDiagram(cdPath, project);
    var pdPath = oldFsPath.replace(/(.*)\\(.*)/, "$1\\problemDiagram.xml")
    this.savePro.saveProblemDiagram(pdPath, project);
  }

  private updateDocument(document: vscode.TextDocument, newContent: string, save?: boolean) {
    //将文件保存到硬盘
    const edit = new vscode.WorkspaceEdit();
    edit.replace(document.uri, new vscode.Range(0, 0, document.lineCount, 0), newContent);
    const editTask = vscode.workspace.applyEdit(edit);
    if (save) {
      editTask.then(() => {
        document.save();
      });
    }
  }
  getExtensionFileVscodeResource(context, relativePath) {
    //webview.asWebviewUri(vscode.Uri.file('/Users/codey/workspace/cat.gif'))
    // const diskPath = vscode.Uri.file(path.join(context.extensionPath, relativePath));
    //return diskPath.with({ scheme: 'vscode-resource' }).toString();
  }

}
