import { Phenomenon } from "./Phenomenon";
export class ProblemDomain {
  no: number;
  name: string;
  shortname: string;
  type: string;
  property: string;
  x: number;
  y: number;
  h: number;
  w: number;
  phes: Phenomenon[];

  getNo() {
    return this.no;
  }
  getName() {
    return this.name;
  }
  getShortname() {
    return this.shortname;
  }
  getX() {
    return this.x;
  }
  getY() {
    return this.y;
  }
  getH() {
    return this.h;
  }
  getW() {
    return this.w;
  }
  getProperty() {
    return this.property;
  }
  getType() {
    return this.type;
  }
  setNo(no) {
    this.no = no;
  }
  setName(name) {
    this.name = name;
  }
  setShortname(shortname) {
    this.shortname = shortname;
  }
  setX(x) {
    this.x = x;
  }
  setY(y) {
    this.y = y;
  }
  setH(h) {
    this.h = h;
  }
  setW(w) {
    this.w = w;
  }
  setProperty(property) {
    this.property = property;
  }
  setType(type) {
    this.type = type;
  }
}
