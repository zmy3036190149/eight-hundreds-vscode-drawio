export class Machine {
  name: string;
  shortname: string;
  x: number;
  y: number;
  h: number;
  w: number;

  getName() {
    return this.name;
  }
  setName(name) {
    this.name = name;
  }

  getShortname() {
    return this.shortname;
  }
  setShortname(shortname) {
    this.shortname = shortname;
  }

  getX() {
    return this.x;
  }
  getY() {
    return this.y;
  }
  getH() {
    return this.h;
  }
  getW() {
    return this.w;
  }

  setX(x) {
    this.x = x;
  }
  setY(y) {
    this.y = y;
  }
  setH(h) {
    this.h = h;
  }
  setW(w) {
    this.w = w;
  }
}
