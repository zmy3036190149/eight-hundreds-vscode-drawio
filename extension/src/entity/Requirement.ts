export class Requirement {
  no: number;
  name: string;
  shortname: string;
  x: number;
  y: number;
  h: number;
  w: number;

  getNo() {
    return this.no;
  }
  setNo(no) {
    this.no = no;
  }
  getName() {
    return this.name;
  }
  setName(name) {
    this.name = name;
  }
  getShortname() {
    return this.shortname;
  }
  setShortname(shortname) {
    this.shortname = shortname;
  }
  getX() {
    return this.x;
  }
  setX(x) {
    this.x = x;
  }
  getY() {
    return this.y;
  }
  setY(y) {
    this.y = y;
  }
  getH() {
    return this.h;
  }
  setH(h) {
    this.h = h;
  }
  getW() {
    return this.w;
  }
  setW(w) {
    this.w = w;
  }
}

