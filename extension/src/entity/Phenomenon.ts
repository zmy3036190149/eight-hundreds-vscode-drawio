export class Phenomenon {
  setNo(no: any) {
    this.no = no;
  }
  setName(name: any) {
    this.name = name;
  }
  setType(type: any) {
    this.type = type;
  }
  setFrom(from: any) {
    this.from = from;
  }
  setTo(to: any) {
    this.to = to;
  }
  getNo() {
    return this.no;
  }
  getName() {
    return this.name;
  }
  getType() {
    return this.type;
  }
  getFrom() {
    return this.from;
  }
  getTo() {
    return this.to;
  }

  no: number; //现象编号
  name: string; //现象名称
  type: string; //现象类型
  from: string; //发送方
  to: string; //接收方
}
