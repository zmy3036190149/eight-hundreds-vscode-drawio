import * as vscode from "vscode";
// import * as WebSocket from 'ws';
import { LSPMessageFactory } from './LSPMessageFactory';
var net = require('net')
var WebSocket = require('ws')
const fs_1 = require('fs')
class pfDocument {
  currentFileName: string
  uri: string
  username = "vscode"
  title: string
  lastVersion: string | undefined
  isOpen = false;
  projectAddress: string | undefined
  version: string | undefined
  messageId = 0
  ws
  newValue
  interval
  saveTimes
  constructor(title, currentFileName) {
    this.title = title
    this.currentFileName = currentFileName
    this.openWebSocket()
  }
  openWebSocket() {
    this.ws = new WebSocket("ws://47.52.116.116:8098/TextLSP");
    var that = this;
    this.ws.onopen = function () {
      console.log(that.title, ' ws connection is open');
      let value = fs_1.readFileSync(that.currentFileName) + ''
      that.register(that.title, 'undefined', value);
    };
    this.ws.onmessage = function (e) {
      var message = JSON.parse(e.data);
      let params = message.params;
      that.lastVersion = params.textDocument.lastVersion;
      if (message.method == "TextDocument/didChange") {
        that.update(message);
      } else if (message.method == "TextDocument/registered") {
        that.registed_new(message);
      }
    };
    this.ws.onerror = function (e) {
      console.log('==============================error==============================', e);
    };
    this.ws.onclose = function (e) {
      console.log("====================close=======================", e);
      that.isOpen = false;
      setTimeout(() => {
        console.log(new Date().getMilliseconds())
        that.openWebSocket();
      }, 3000);
    };
  }
  register(title, version: string, text) {
    version =
      version == undefined
        ? 'undefined'
        : version.replace(':', '_').replace(':', '_')
    //console.log(version);
    this.uri = this.username + title + version
    let textDocument = {
      uri: this.uri,
      version: 0,
      text: text,
    }
    let params = {
      textDocument: textDocument,
    }
    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      'TextDocument/didOpen',
      params
    )
    this.sendMessage2(this.ws, message)
    this.projectAddress = title
    this.version = version
    this.newValue = text
    var that = this
    this.interval = setInterval(function () { that.didSave() }, 1000)
  }
  register_new(title: string, version: string) {
    version = version.replace(':', '_').replace(':', '_')
    this.uri = this.username + title + version;
    let textDocument = {
      uri: this.uri,
      projectAddress: title,
      projectVersion: version,
      version: 0,
    }
    let params = {
      textDocument: textDocument,
    }
    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      'TextDocument/register',
      params
    )
    this.sendMessage2(this.ws, message)
    this.projectAddress = title
    this.version = version
  }
  sendMessage2(ws, message) {
    let i = 0
    let that = this
    let interval1 = setInterval(function () {
      if (ws.readyState == WebSocket.OPEN) {
        clearInterval(interval1)
        ws.send(JSON.stringify(message))
      } else if (ws.readyState == WebSocket.CLOSED) {
        // // console.log("diagram.ws.readyState=CLOSED ", ws.readyState);
      } else if (ws.readyState == WebSocket.CLOSING) {
        // // console.log("diagram.ws.readyState=CLOSING ", ws.readyState);
      } else if (ws.readyState == WebSocket.CONNECTING) {
        // // console.log("diagram.ws.readyState=CONNECTING ", ws.readyState);
      }
    }, 1000)
  }
  registed_new(message) {
    this.isOpen = true;
    let params = message.params;
    this.newValue = params.text;
  }
  public didSave(): void {
    let error = false
    let value = fs_1.readFileSync(this.currentFileName) + ''
    if (
      !error &&
      this.isOpen
    ) {
      let params = {
        textDocument: {
          uri: this.uri,
          version: null,
          lastVersion: this.lastVersion,
          changeTime: new Date().getTime(),
        },
        text: value,
      }
      let message = LSPMessageFactory.getMessageNoFraming(
        this.messageId++,
        'TextDocument/didSave',
        params
      )
      this.sendMessage2(this.ws, message)
      this.newValue = value
    }
    this.saveTimes = (this.saveTimes + 1) % 10
  }
  update(jsonMessage) {
    let params = jsonMessage.params;
    this.newValue = params.text;
    fs_1.writeFileSync(this.currentFileName, this.newValue);
  }
}
export class Synchronization extends vscode.Disposable {
  private _disposables: vscode.Disposable[] = [];
  _context: vscode.ExtensionContext
  pfs = new Map()
  lastFileName
  constructor(context: vscode.ExtensionContext) {
    super(() => this.dispose());
    this._context = context

    let that = this
    setInterval(() => {
      let currentFileName = vscode.window.activeTextEditor ? vscode.window.activeTextEditor.document.fileName : ""
      if (this.lastFileName !== currentFileName && currentFileName !== "") {
        this.lastFileName = currentFileName
      }
      let fileNameIndex = currentFileName.lastIndexOf("\\");
      let pointIndex = currentFileName.indexOf(".pf");
      if (pointIndex == -1) {
        return;
      }
      let title = currentFileName.slice(fileNameIndex + 1, pointIndex)
      let uri = currentFileName.replace(':\\', '_')
      while (uri.indexOf('\\') != -1) {
        uri = uri.replace('\\', "_")
      }
      let ind = uri.indexOf('.');
      uri = ind == -1 ? uri : uri.slice(0, ind)
      if (that.pfs.get(uri) != null) {
        //console.log("自动保存", uri)
        that.pfs.get(uri).didSave();
      } else {
        console.log("首次打开", uri, title)
        that.pfs.set(uri, new pfDocument(title, currentFileName))
      }
    }, 1000);
  }
  dispose() {
    this._disposables && this._disposables.length && this._disposables.map(d => d.dispose());
  }
}