import * as vscode from "vscode";
var XMLWriter = require('xml-writer')
const viewType = "pff.editor";
export class PFEditorProvider implements vscode.CustomTextEditorProvider {
  static register(context: vscode.ExtensionContext) {
    const provider = new PFEditorProvider(context);
    const providerRegistration = vscode.window.registerCustomEditorProvider(viewType, provider);
    return providerRegistration;
  }
  constructor(private readonly context: vscode.ExtensionContext) { }

  resolveCustomTextEditor(
    document: vscode.TextDocument,
    webviewPanel: vscode.WebviewPanel,
    token: vscode.CancellationToken
  ): void | Thenable<void> {
  }

}
