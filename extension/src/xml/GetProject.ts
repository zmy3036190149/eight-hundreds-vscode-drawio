import * as fs from "fs";
import { Constraint } from "../entity/Constraint";
import { Interface } from "../entity/Interface";
import { Machine } from "../entity/Machine";
import { Phenomenon } from "../entity/Phenomenon";
import { ProblemDomain } from "../entity/ProblemDomain";
import { Project } from "../entity/Project";
import { Reference } from "../entity/Reference";
import { Requirement } from "../entity/Requirement";
import { RequirementPhenomenon } from "../entity/RequirementPhenomenon";
var xmlreader = require("xmlreader");
export class GetProject {
  read(url) {
    return new Promise((resolve, reject) => {
      fs.readFile(url, 'utf8', (err, data) => {
        if (err) return reject(err)
        //console.log(url, data)
        resolve(data)
      })
    })
  }
  readxml(data) {
    return new Promise((resolve, reject) => {
      xmlreader.read(data, (err, response) => {
        if (err) return reject(err)
        //console.log("readxml", response)
        resolve(response)
      })
    })
  }
  getProject(file) {
    var project = new Project()
    var cdPath = file.replace(/(.*)\\(.*)/, "$1\\ContextDiagram.xml")
    var pdPath = file.replace(/(.*)\\(.*)/, "$1\\ProblemDiagram.xml")
    return this.read(file)
      .then((data) => {
        return this.readxml(data)
      })
      .then((data) => {
        // get project.title        
        //console.log("project.title.text:", (data as any).project.title.text())
        project.init((data as any).project.title.text())
        return this.read(cdPath)
      })
      .then((data) => {
        return this.readxml(data)
      })
      .then((data) => {
        // get project.contextDiagram        
        var contextDiagram = project.contextDiagram
        contextDiagram.title = (data as any).ContextDiagram.title.text()
        let cdxml = (data as any).ContextDiagram
        let mac = new Machine()
        mac.name = cdxml.Machine.attributes().machine_name
        mac.shortname = cdxml.Machine.attributes().machine_shortname
        let loc = cdxml.Machine.attributes().machine_locality.split(",")
        mac.x = parseInt(loc[0])
        mac.y = parseInt(loc[1])
        mac.h = parseInt(loc[2])
        mac.w = parseInt(loc[3])
        contextDiagram.machine = mac
        if (cdxml.ProblemDomain.GivenDomain.Element) {
          cdxml.ProblemDomain.GivenDomain.Element.each(function (i, Element) {
            let pd = new ProblemDomain()
            pd.no = Element.attributes().problemdomain_no
            pd.name = Element.attributes().problemdomain_name
            pd.shortname = Element.attributes().problemdomain_shortname
            pd.type = Element.attributes().problemdomain_type
            pd.property = "GivenDomain"
            let loc = Element.attributes().problemdomain_locality.split(",");
            pd.x = parseInt(loc[0])
            pd.y = parseInt(loc[1])
            pd.w = parseInt(loc[2])
            pd.h = parseInt(loc[3])
            contextDiagram.problemDomainList.push(pd)
          })
        }
        if (cdxml.ProblemDomain.DesignDomain.Element) {
          cdxml.ProblemDomain.DesignDomain.Element.each(function (i, Element) {
            let pd = new ProblemDomain()
            pd.no = Element.attributes().problemdomain_no
            pd.name = Element.attributes().problemdomain_name
            pd.shortname = Element.attributes().problemdomain_shortname
            let loc = Element.attributes().problemdomain_locality.split(",");
            pd.x = parseInt(loc[0])
            pd.y = parseInt(loc[1])
            pd.h = parseInt(loc[2])
            pd.w = parseInt(loc[3])
            contextDiagram.problemDomainList.push(pd)
          })
        }
        if (cdxml.Interface.Element) {
          cdxml.Interface.Element.each(function (i, Element) {
            let inte = new Interface()
            inte.no = Element.attributes().interface_no
            inte.name = Element.attributes().interface_name
            inte.from = Element.attributes().interface_from
            inte.to = Element.attributes().interface_to
            let loc = Element.attributes().interface_locality.split(",");
            inte.x1 = parseInt(loc[0])
            inte.y1 = parseInt(loc[1])
            inte.x2 = parseInt(loc[2])
            inte.y2 = parseInt(loc[3])
            inte.phenomenonList = []
            //<Phenomenon phenomenon_no="1" phenomenon_name="phe1" phenomenon_type="event" phenomenon_from="PD2" phenomenon_to="M1"/>
            if (Element.Phenomenon) {
              Element.Phenomenon.each(function (i, PheElement) {
                let phe = new Phenomenon()
                phe.no = PheElement.attributes().phenomenon_no
                phe.name = PheElement.attributes().phenomenon_name
                phe.type = PheElement.attributes().phenomenon_type
                phe.from = PheElement.attributes().phenomenon_from
                phe.to = PheElement.attributes().phenomenon_to
                inte.phenomenonList.push(phe)
              })
            }
            contextDiagram.interfaceList.push(inte)
          })
        }
        return this.read(pdPath)
      })
      .then((data) => {
        return this.readxml(data)
      })
      .then((data) => {
        // get project.problemDiagram
        // console.log(data)
        var problemDiagram = project.problemDiagram
        problemDiagram.title = (data as any).ProblemDiagram.title.text()
        let pdxml = (data as any).ProblemDiagram
        if (pdxml.Requirement.Element) {
          pdxml.Requirement.Element.each(function (i, Element) {
            let req = new Requirement()
            req.no = Element.attributes().requirement_no
            req.name = Element.attributes().requirement_context
            let loc = Element.attributes().requirement_locality.split(",");
            req.x = parseInt(loc[0])
            req.y = parseInt(loc[1])
            req.h = parseInt(loc[2])
            req.w = parseInt(loc[3])
            problemDiagram.requirementList.push(req)
          })
        }
        if (pdxml.Constraint.Element) {
          pdxml.Constraint.Element.each(function (i, Element) {
            let con = new Constraint()
            con.no = Element.attributes().constraint_no
            con.name = Element.attributes().constraint_name
            con.from = Element.attributes().constraint_from
            con.to = Element.attributes().constraint_to
            let loc = Element.attributes().constraint_locality.split(",");
            con.x1 = parseInt(loc[0])
            con.y1 = parseInt(loc[1])
            con.x2 = parseInt(loc[2])
            con.y2 = parseInt(loc[3])
            con.phenomenonList = []
            if (Element.Phenomenon) {
              Element.Phenomenon.each(function (i, PheElement) {
                let phe = new RequirementPhenomenon()
                phe.no = PheElement.attributes().phenomenon_no
                phe.name = PheElement.attributes().phenomenon_name
                phe.type = PheElement.attributes().phenomenon_type
                phe.from = PheElement.attributes().phenomenon_from
                phe.to = PheElement.attributes().phenomenon_to
                phe.requirement = PheElement.attributes().phenomenon_requirement
                con.phenomenonList.push(phe)
              })
            }
            problemDiagram.constraintList.push(con)
          })
        }
        if (pdxml.Reference.Element) {
          pdxml.Reference.Element.each(function (i, Element) {
            let ref = new Reference()
            ref.no = Element.attributes().reference_no
            ref.name = Element.attributes().reference_name
            ref.from = Element.attributes().reference_from
            ref.to = Element.attributes().reference_to
            ref.description = Element.attributes().reference_description
            let loc = Element.attributes().reference_locality.split(",");
            ref.x1 = parseInt(loc[0])
            ref.y1 = parseInt(loc[1])
            ref.x2 = parseInt(loc[2])
            ref.y2 = parseInt(loc[3])
            ref.phenomenonList = []
            if (Element.Phenomenon) {
              Element.Phenomenon.each(function (i, PheElement) {
                let phe = new RequirementPhenomenon()
                phe.no = PheElement.attributes().phenomenon_no
                phe.name = PheElement.attributes().phenomenon_name
                phe.type = PheElement.attributes().phenomenon_type
                phe.from = PheElement.attributes().phenomenon_from
                phe.to = PheElement.attributes().phenomenon_to
                phe.requirement = PheElement.attributes().phenomenon_requirement
                ref.phenomenonList.push(phe)
              })
            }
            problemDiagram.referenceList.push(ref)
          })
        }
        return new Promise((resolve, reject) => { return resolve(project) })
      }).catch((error) => {
        return new Promise((resolve, reject) => { return resolve(project) })
      })
  }
}