import * as fs from "fs";
var XMLWriter = require('xml-writer')
export class SaveProject {
  public saveProject(proPath, project) {
    var xw = new XMLWriter;
    xw.startDocument();
    xw.startElement('project');
    xw.startElement('title');
    xw.text(project.title);
    xw.endElement('title');
    xw.startElement('fileList');
    xw.startElement('ContextDiagram');
    xw.text('ContextDiagram');
    xw.endElement('ContextDiagram');
    xw.startElement('ProblemDiagram');
    xw.text('ProblemDiagram');
    xw.endElement('ProblemDiagram');
    xw.endElement('fileList');
    xw.endDocument();
    this.write(proPath, xw.toString())
  }
  public saveContextDiagram(cdPath, project) {
    var xw = new XMLWriter;
    xw.startDocument();
    xw.startElement('ContextDiagram');
    xw.startElement('title');
    xw.text('ContextDiagram');
    xw.endElement('title');
    var machine = project.contextDiagram.machine
    if (machine) {
      xw.startElement('Machine');
      xw.writeAttribute("machine_name", machine.name)
      xw.writeAttribute("machine_shortname", machine.shortname)
      xw.writeAttribute("machine_locality", machine.x + "," + machine.y + "," + machine.h + "," + machine.w)
      xw.endElement('Machine');
    }

    xw.startElement('ProblemDomain');
    xw.startElement('GivenDomain');
    if (project.contextDiagram.problemDomainList.length > 0)
      for (let pd of project.contextDiagram.problemDomainList) {
        if (pd.property === "GivenDomain") {
          xw.startElement('Element');
          xw.writeAttribute("problemdomain_no", pd.no)
          xw.writeAttribute("problemdomain_name", pd.name)
          xw.writeAttribute("problemdomain_shortname", pd.shortname)
          xw.writeAttribute("problemdomain_type", pd.type)
          xw.writeAttribute("problemdomain_locality", pd.x + "," + pd.y + "," + pd.h + "," + pd.w)
          xw.endElement('Element');
        }
      }
    xw.endElement('GivenDomain');
    xw.startElement('DesignDomain');
    if (project.contextDiagram.problemDomainList.length > 0)
      for (let pd of project.contextDiagram.problemDomainList) {
        if (pd.property === "DesignDomain") {
          xw.startElement('Element');
          xw.writeAttribute("problemdomain_no", pd.no)
          xw.writeAttribute("problemdomain_name", pd.name)
          xw.writeAttribute("problemdomain_shortname", pd.shortname)
          xw.writeAttribute("problemdomain_type", pd.type)
          xw.writeAttribute("problemdomain_locality", pd.x + "," + pd.y + "," + pd.h + "," + pd.w)
          xw.endElement('Element');
        }
      }
    xw.endElement('DesignDomain');
    xw.endElement('ProblemDomain');
    xw.startElement('Interface');
    if (project.contextDiagram.interfaceList.length > 0)
      for (let item of project.contextDiagram.interfaceList) {
        xw.startElement('Element');
        xw.writeAttribute("interface_no", item.no)
        xw.writeAttribute("interface_name", item.name)
        xw.writeAttribute("interface_from", item.from)
        xw.writeAttribute("interface_to", item.to)
        xw.writeAttribute("interface_description", item.description)
        xw.writeAttribute("interface_locality", item.x1 + "," + item.y1 + "," + item.x2 + "," + item.y2)
        if (item.phenomenonList.length > 0)
          for (let phe of item.phenomenonList) {
            xw.startElement("Phenomenon")
            xw.writeAttribute("phenomenon_no", phe.no)
            xw.writeAttribute("phenomenon_name", phe.name)
            xw.writeAttribute("phenomenon_type", phe.type)
            xw.writeAttribute("phenomenon_from", phe.from)
            xw.writeAttribute("phenomenon_to", phe.to)
            xw.endElement("Phenomenon")
          }
        xw.endElement('Element');
      }
    xw.endElement('Interface');
    xw.endDocument();
    // console.log(xw.toString());
    this.write(cdPath, xw.toString())
  }
  public saveProblemDiagram(pdPath, project) {
    var xw = new XMLWriter;
    xw.startDocument();
    xw.startElement('ProblemDiagram');
    xw.startElement('ContextDiagram');
    xw.text('ContextDiagram');
    xw.endElement('ContextDiagram');
    xw.startElement('title');
    xw.text('ProblemDiagram');
    xw.endElement('title');
    xw.startElement('Requirement');
    if (project.problemDiagram.requirementList.length > 0)
      for (let req of project.problemDiagram.requirementList) {

        xw.startElement('Element');
        xw.writeAttribute("requirement_no", req.no)
        xw.writeAttribute("requirement_context", req.name)
        xw.writeAttribute("requirement_shortname", req.shortname)
        xw.writeAttribute("requirement_locality", req.x + "," + req.y + "," + req.h + "," + req.w)
        xw.endElement('Element');

      }
    xw.endElement('Requirement');
    xw.startElement('Constraint');
    if (project.problemDiagram.constraintList.length > 0)
      for (let item of project.problemDiagram.constraintList) {
        xw.startElement('Element');
        xw.writeAttribute("constraint_no", item.no)
        xw.writeAttribute("constraint_name", item.name)
        xw.writeAttribute("constraint_from", item.from)
        xw.writeAttribute("constraint_to", item.to)
        xw.writeAttribute("constraint_description", item.description)
        xw.writeAttribute("constraint_locality", item.x1 + "," + item.y1 + "," + item.x2 + "," + item.y2)
        if (item.phenomenonList.length > 0)
          for (let phe of item.phenomenonList) {
            xw.startElement("Phenomenon")
            xw.writeAttribute("phenomenon_no", phe.no)
            xw.writeAttribute("phenomenon_name", phe.name)
            xw.writeAttribute("phenomenon_type", phe.type)
            xw.writeAttribute("phenomenon_from", phe.from)
            xw.writeAttribute("phenomenon_to", phe.to)
            xw.writeAttribute("phenomenon_requirement", phe.requirement)
            xw.endElement("Phenomenon")
          }
        xw.endElement('Element');
      }
    xw.endElement('Constraint');

    xw.startElement('Reference');
    if (project.problemDiagram.referenceList.length > 0)
      for (let item of project.problemDiagram.referenceList) {
        xw.startElement('Element');
        xw.writeAttribute("reference_no", item.no)
        xw.writeAttribute("reference_name", item.name)
        xw.writeAttribute("reference_from", item.from)
        xw.writeAttribute("reference_to", item.to)
        xw.writeAttribute("reference_description", item.description)
        xw.writeAttribute("reference_locality", item.x1 + "," + item.y1 + "," + item.x2 + "," + item.y2)
        if (item.phenomenonList.length > 0)
          for (let phe of item.phenomenonList) {
            xw.startElement("Phenomenon")
            xw.writeAttribute("phenomenon_no", phe.no)
            xw.writeAttribute("phenomenon_name", phe.name)
            xw.writeAttribute("phenomenon_type", phe.type)
            xw.writeAttribute("phenomenon_from", phe.from)
            xw.writeAttribute("phenomenon_to", phe.to)
            xw.writeAttribute("phenomenon_requirement", phe.requirement)
            xw.endElement("Phenomenon")
          }
        xw.endElement('Element');
      }
    xw.endElement('Reference');
    xw.endDocument();
    // console.log(xw.toString());
    this.write(pdPath, xw.toString())
  }
  write(newPath, str) {
    // 创建一个可以写bai入的流，写入到文件 output.txt 中
    var writerStream = fs.createWriteStream(newPath);

    // 使用 utf8 编码写入数据
    writerStream.write(str, 'UTF8');

    // 标记文件末尾
    writerStream.end();

    // 处理流事件 --> data, end, and error
    writerStream.on('finish', function () {
      // console.log("写入完成。");
    });

    writerStream.on('error', function (err) {
      console.log(err.stack);
    });
  }
}