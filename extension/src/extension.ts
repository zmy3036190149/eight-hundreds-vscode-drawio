import * as vscode from 'vscode';
import { DrawIOEditorProvider } from './DrawIOEditorProvider';
import { LanguageServer } from "./languageServer/languageServer";
// import { PFEditorProvider } from './synchronization/PFEditorProvider';
import { Synchronization } from "./synchronization/synchronization";
export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(DrawIOEditorProvider.register(context));
	// context.subscriptions.push(PFEditorProvider.register(context));

	//launch ls for syntax
	new LanguageServer(context);

	//launch backend for syn
	// portIsOccupied(8098);

	//打开文件时连接同步服务器
	context.subscriptions.push(new Synchronization(context));
}