import { RequirementPhenomenon } from '../entity/RequirementPhenomenon';
export class RequirementPhenomenonOperation {
  static copyPhenomenon(old) {
    let phe = new RequirementPhenomenon();
    phe.no = old.no
    phe.name = old.name
    phe.type = old.type
    phe.from = old.from
    phe.to = old.to
    phe.requirement = old.requirement
    phe.constraint = old.constraint
    return phe
  }
  static newPhenomenon(no, name, type, from, to, requirement, constraint) {
    let phe = new RequirementPhenomenon();
    phe.no = no
    phe.name = name
    phe.type = type
    phe.from = from
    phe.to = to
    phe.requirement = requirement
    phe.constraint = constraint
    return phe
  }
}
