import { Phenomenon } from '../entity/Phenomenon';
export class PhenomenonOperation {
  static newPhenomenon(no, name, type, from, to) {
    let phe = new Phenomenon();
    phe.no = no;
    phe.name = name;
    phe.type = type;
    phe.from = from;
    phe.to = to;
    return phe;
  }
}
