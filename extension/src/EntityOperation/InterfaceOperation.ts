import { Interface } from "../entity/Interface";
export class InterfaceOperation {

  static newShape(
    no,
    name,
    description,
    from,
    to,
    phenomenonList,
    x1,
    y1,
    x2,
    y2
  ) {
    let int = new Interface();
    int.no = no;
    int.name = name;
    int.description = description;
    int.from = from;
    int.to = to;
    if (phenomenonList) int.phenomenonList = phenomenonList;
    else int.phenomenonList = [];
    int.x1 = x1;
    int.y1 = y1;
    int.x2 = x2;
    int.y2 = y2;
    return int;
  }
  static newShapeWithOld(old, description) {
    let int = new Interface();
    int.no = old.no;
    int.name = old.name;
    int.description = description;
    int.from = old.from;
    int.to = old.to;
    if (old.phenomenonList) int.phenomenonList = old.phenomenonList;
    else int.phenomenonList = [];
    int.x1 = old.x1;
    int.y1 = old.y1;
    int.x2 = old.x2;
    int.y2 = old.y2;
    return int;
  }

  static copyShape(old) {
    let int = new Interface();
    int.no = old.no;
    int.name = old.name;

    int.from = old.from;
    int.to = old.to;
    if (old.phenomenonList) int.phenomenonList = old.phenomenonList;
    else int.phenomenonList = [];
    int.x1 = old.x1;
    int.y1 = old.y1;
    int.x2 = old.x2;
    int.y2 = old.y2;
    Interface.setDescription1(int);
    return int;
  }
}