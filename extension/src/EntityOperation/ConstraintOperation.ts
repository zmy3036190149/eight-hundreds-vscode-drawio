import { Constraint } from "../entity/Constraint";
export class ConstraintOperation {
  static newShape(
    no,
    name,
    description,
    from,
    to,
    phenomenonList,
    x1,
    y1,
    x2,
    y2
  ) {
    let con = new Constraint();
    con.no = no;
    con.name = name;
    con.description = description;
    con.from = from;
    con.to = to;
    if (phenomenonList) con.phenomenonList = phenomenonList;
    else con.phenomenonList = [];
    con.x1 = x1;
    con.y1 = y1;
    con.x2 = x2;
    con.y2 = y2;
    return con;
  }
  static newShapeWithOld(old, description) {
    let con = new Constraint();
    con.no = old.no;
    con.name = old.name;
    con.from = old.from;
    con.to = old.to;
    con.x1 = old.x1;
    con.y1 = old.y1;
    con.x2 = old.x2;
    con.y2 = old.y2;
    con.description = description;
    con.phenomenonList = old.phenomenonList;
    return con;
  }
  static copyShape(old) {
    let con = new Constraint();
    con.no = old.no;
    con.name = old.name;
    con.from = old.from;
    con.to = old.to;
    con.x1 = old.x1;
    con.y1 = old.y1;
    con.x2 = old.x2;
    con.y2 = old.y2;
    if (old.phenomenonList) con.phenomenonList = old.phenomenonList;
    else con.phenomenonList = [];
    Constraint.setDescription1(con);
    return con;
  }
}