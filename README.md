# 环境

node10.

# 运行方法

yarn install 安装依赖

yarn compile 编译

/out/extension.js F5 打开新窗口调试

打开目录/testDir

文本 .pf 图形 .project.project

# 20200901 代码改动

## 左侧侧栏主要改动：

src\main\webapp\js\mxgraph\Sidebar.js
（1）addGeneralPalette 函数里面只保留以下 6 个创建语句，表示我们所需的 6 种图形符号
（2）addMiscPalette 函数注释掉最后两行代码，表示隐藏 Misc 模块
（3）addAdvancedPalette 函数注释掉最后两行代码，表示隐藏 Advanced 模块
（4）addUmlPalette 函数注释掉最后一行代码，表示隐藏 Uml 模块
（5）addBpmnPalette 函数注释掉最后两行代码，表示隐藏 Bpmn 模块

src\main\webapp\js\mxgraph\Shape.js 中添加有关 machine 图形的代码

在 drawio\src\main\webapp\js\diagramly\sidebar 目录中，分别注释掉 Sidebar-Arrows2.js、Sidebar-Basic.js、Sidebar-BPMN.js、Sidebar-ER.js、Sidebar-Flowchart.js 五个文件中的相应内容，表示隐藏剩下的 6 个模块

因为 drawio 的版本不一样，替换 Sidebar.js 后又改了几个地方
（1）注释掉 295-297 三行代码
var temp = this.graph2.cloneCells(cells);
this.editorUi.insertHandler(temp, null, this.graph2.model);
this.graph2.addCells(temp);
（2）。。。

## 右侧侧栏改动

更改 drawio\src\main\webapp\js\mxgraph\ForMat.js
（1）注释掉 Arrange,Style 和 Text 模块
（2）添加 Phenomenon 模块

## 添加 ant 构建相关文件

添加 ant 构建相关文件，并根据版本不同做了相关处理,例如添加版本号，删除不存在的文件的配置
