import * as WebSocket from "ws";
import { Constraint } from "../entity/Constraint";
import { Interface } from "../entity/Interface";
import { Machine } from "../entity/Machine";
import { Phenomenon } from "../entity/Phenomenon";
import { ProblemDiagram } from "../entity/ProblemDiagram";
import { ProblemDomain } from "../entity/ProblemDomain";
import { Project } from "../entity/Project";
import { Reference } from "../entity/Reference";
import { Requirement } from "../entity/Requirement";
import { RequirementPhenomenon } from "../entity/RequirementPhenomenon";
import { ConstraintOperation } from "../EntityOperation/ConstraintOperation";
import { InterfaceOperation } from "../EntityOperation/InterfaceOperation";
import { MachineOperation } from "../entityOperation/MachineOperation";
import { PhenomenonOperation } from "../entityOperation/PhenomenonOperation";
import { ProblemDomainOperation } from "../entityOperation/ProblemDomainOperation";
import { ReferenceOperation } from "../EntityOperation/ReferenceOperation";
import { RequirementOperation } from "../entityOperation/RequirementOperation";
import { RequirementPhenomenonOperation } from "../entityOperation/RequirementPhenomenonOperation";
import { DiagramContentChangeEvent } from "../LSP/DiagramContentChangeEvent";
import { DiagramContentChangeEventFactory } from "../LSP/DiagramContentChangeEventFactory";
import { LSPMessageFactory } from "../LSP/LSPMessageFactory";
export class DrawGraphService {
  keyWords = [
    "M",
    "C",
    "B",
    "X",
    "D",
    "P",
    "R",
    "?",
    "phenomenon",
    "value",
    "event",
    "state",
  ];
  PhysicalPropertys = ["GivenDomain", "DesignDomain"];
  DomainTypes = ["Causal", "Biddable", "Lexical"];
  phenomenonTypes = ["event", "state", "value"];
  NodeTypes = ["machine", "domain", "requirement"];
  LinkTypes = ["interface", "constraint", "reference"];

  isOpen: boolean;
  heartbeatMechanismInterval: NodeJS.Timer;
  moveTimes: any;

  //通信相关
  ws
  messageId = 0;
  interval1;

  //项目相关（状态，数据）
  uri: string;
  username = "vscode"
  projectAddress: string;
  version: string;
  lastVersion: string;
  project: Project;
  isProjectNull = true;

  //当前选中元素（类型）
  selectedType: string;
  machine: Machine;
  problemDomain: ProblemDomain;
  interface: Interface;
  requirement: Requirement;
  reference: Reference;
  constraint: Constraint;
  phenomenonList = new Array<Phenomenon>();
  requirementPhenomenonList = new Array<RequirementPhenomenon>();

  // 编号相关
  problemdomain_no = 1;
  requirement_no = 1;
  interface_no = 1;
  reference_no = 1;
  constraint_no = 1;
  phenomenon_no = 1;
  link_name_no = 1;

  // Phenomenon 相关
  initiatorList = new Array<String>();

  // Requirement Phenomenon相关  
  initiator;
  receiverList;
  initiator_receiverList;
  initiator_or_receiverList;

  //Requirement Phenomenon提示相关
  interfacePhes;
  ontologyPhes;
  interface_ontologyPhes;

  constructor() {
    this.project = new Project();
    this.project.init("test")
    this.openWebSocket();
  }

  //========================================WebSocket========================================
  openWebSocket() {
    this.ws = new WebSocket("ws://47.52.116.116:8098/DiagramLSP");
    var that = this;

    this.ws.onopen = function () {
      that.heartbeatMechanismInterval = setInterval(function () {
        that.heartbeatMechanism();
      }, 30000);
    };

    this.ws.onmessage = function (e) {
      var message = JSON.parse(e.data);
      let params = message.params;
      that.lastVersion = params.diagram.lastVersion;
      if (message.method == "Diagram/didChange") {
        let diagramContentChangeEvents = params.contentChanges;
        that.wsChange(diagramContentChangeEvents);
        setTimeout(function () {
          that.postMessage();
        }, 500)
        if (diagramContentChangeEvents[0].shapeType != "project") return;
        let T0 = params.diagram.T0 == undefined ? 0 : params.diagram.T0;
        let T1 = params.diagram.T1 == undefined ? 0 : params.diagram.T1;
        let T2 = params.diagram.T2 == undefined ? 0 : params.diagram.T2;
        let T3 = params.diagram.T3 == undefined ? 0 : params.diagram.T3;
        let T4 = new Date().getTime();
        let Flag1 =
          params.diagram.Flag1 == undefined ? 0 : params.diagram.Flag1;
        let Flag2 =
          params.diagram.Flag2 == undefined ? 0 : params.diagram.Flag2;
        let Flag3 =
          params.diagram.Flag3 == undefined ? 0 : params.diagram.Flag3;
        that.sendTimeRecoder(
          that.lastVersion,
          T0,
          T1,
          T2,
          T3,
          T4,
          Flag1,
          Flag2,
          Flag3
        );
      } else if (message.method == "Diagram/changeLastVersion") {
      } else if (message.method == "Diagram/registered") {
        if (that.isOpen == true) {
          // that.registed_second(message);
          that.registed_new(message);
          console.log("重复收到 registered ");
          return;
        }
        that.registed_new(message);
        that.isOpen = true;
      }
    };

    this.ws.onerror = function (e) {
      // // //console.log('========================================error========================================', e);
    };

    this.ws.onclose = function (e) {
      // // console.log("========================================close========================================", e);
      that.openWebSocket();
      let interval_open = setInterval(function () {
        clearInterval(interval_open);
        that.register(that.projectAddress, that.version, this.project);
      }, 1000);
    };
  }
  //----------接收消息 new ---------
  registed_new(message) {
    console.log(message)
    this.isOpen = true;
    let params = message.params;
    let diagramContentChangeEvent = <DiagramContentChangeEvent>(
      params.contentChanges[0]
    );
    let pro: Project = diagramContentChangeEvent.newProject;
    let newProject = new Project();
    newProject.initProject(pro);
    this.setProject(newProject);
    console.log("registed_new")
    this.drawDiagram(this.project);
    this.isProjectNull = false;
  }
  registed_second(message) {
    this.isOpen = true;
    let params = message.params;
    let diagramContentChangeEvent = <DiagramContentChangeEvent>(
      params.contentChanges[0]
    );
    this.wsProject(diagramContentChangeEvent);
    this.isProjectNull = false;
  }
  wsChange(diagramContentChangeEvents: DiagramContentChangeEvent[]) {
    // console.log(diagramContentChangeEvents);
    let removeEvents: DiagramContentChangeEvent[]
    for (let diagramContentChangeEvent of diagramContentChangeEvents) {
      let changeType = diagramContentChangeEvent.changeType;
      if (changeType == "delete") {
        this.wsChange1(diagramContentChangeEvent);
        removeEvents.push(diagramContentChangeEvent)
      }
    }
    for (let item of removeEvents) {
      diagramContentChangeEvents.filter(dcc => dcc != item);
    }
    removeEvents = []
    for (let diagramContentChangeEvent of diagramContentChangeEvents) {
      let shapeType = diagramContentChangeEvent.shapeType;
      if (this.NodeTypes.indexOf(shapeType)) {
        this.wsChange1(diagramContentChangeEvent);
        removeEvents.push(diagramContentChangeEvent)
      }
    }
    for (let item of removeEvents) {
      diagramContentChangeEvents.filter(dcc => dcc != item);
    }
    removeEvents = []
    for (let diagramContentChangeEvent of diagramContentChangeEvents) {
      let shapeType = diagramContentChangeEvent.shapeType;
      if (this.LinkTypes.indexOf(shapeType)) {
        this.wsChange1(diagramContentChangeEvent);
        removeEvents.push(diagramContentChangeEvent)
      }
    }
    for (let item of removeEvents) {
      diagramContentChangeEvents.filter(dcc => dcc != item);
    }
    removeEvents = []
    if (diagramContentChangeEvents.length != 0) {
      console.log(diagramContentChangeEvents)
    }
  }

  wsChange1(diagramContentChangeEvent: DiagramContentChangeEvent) {
    let shapeType = diagramContentChangeEvent.shapeType;
    switch (shapeType) {
      case "mac":
        this.wsMachine(diagramContentChangeEvent);
        break;
      case "pro":
        this.wsProblemDomain(diagramContentChangeEvent);
        break;
      case "req":
        this.wsRequirement(diagramContentChangeEvent);
        break;
      case "int":
        this.wsInterface(diagramContentChangeEvent);
        break;
      case "ref":
        this.wsReference(diagramContentChangeEvent);
        break;
      case "con":
        this.wsConstraint(diagramContentChangeEvent);
        break;
      case "project":
        this.wsProject(diagramContentChangeEvent);
    }

  }
  postMessage() {
    var vscode
    if (vscode.postMessage) {
      vscode.postMessage({
        event: 'autosaveProject',
        data: this.project
      })
      //console.log('autosaveProject')
    }
  }
  wsMachine(diagramContentChangeEvent) {
    let changeType = diagramContentChangeEvent.changeType;
    let oldShape = diagramContentChangeEvent.oldShape;
    let newShape = diagramContentChangeEvent.newShape;
    switch (changeType) {
      case "add":
        this.drawMachinews(newShape);
        break;
      case "delete":
        this.deleteMachinews(oldShape);
        break;
      case "change":
        this.changeMachinews(oldShape, newShape);
        break;
    }
  }
  wsProblemDomain(diagramContentChangeEvent) {
    let changeType = diagramContentChangeEvent.changeType;
    let oldShape = diagramContentChangeEvent.oldShape;
    let newShape = diagramContentChangeEvent.newShape;
    switch (changeType) {
      case "add":
        this.drawProblemDomainws(newShape);
        break;
      case "delete":
        this.deleteProblemDomainws(oldShape);
        break;
      case "change":
        this.changeProblemDomainws(oldShape, newShape);
        break;
    }
  }
  wsRequirement(diagramContentChangeEvent) {
    let changeType = diagramContentChangeEvent.changeType;
    let oldShape = diagramContentChangeEvent.oldShape;
    let newShape = diagramContentChangeEvent.newShape;
    switch (changeType) {
      case "add":
        this.drawRequirementws(newShape);
        break;
      case "delete":
        this.deleteRequirementws(oldShape);
        break;
      case "change":
        this.changeRequirementws(oldShape, newShape);
        break;
    }
  }
  wsInterface(diagramContentChangeEvent) {
    let changeType = diagramContentChangeEvent.changeType;
    let oldShape = diagramContentChangeEvent.oldShape;
    let newShape = diagramContentChangeEvent.newShape;
    switch (changeType) {
      case "add":
        this.drawInterfacews(newShape);
        break;
      case "delete":
        this.deleteInterfacews(oldShape);
        break;
      case "change":
        this.changeInterfacews(oldShape, newShape);
        break;
    }
  }
  wsReference(diagramContentChangeEvent) {
    let changeType = diagramContentChangeEvent.changeType;
    let oldShape = diagramContentChangeEvent.oldShape;
    let newShape = diagramContentChangeEvent.newShape;
    switch (changeType) {
      case "add":
        this.drawReferencews(newShape);
        break;
      case "delete":
        this.deleteReferencews(oldShape);
        break;
      case "change":
        this.changeReferencews(oldShape, newShape);
        break;
    }
  }
  wsConstraint(diagramContentChangeEvent) {
    let changeType = diagramContentChangeEvent.changeType;
    let oldShape = diagramContentChangeEvent.oldShape;
    let newShape = diagramContentChangeEvent.newShape;
    switch (changeType) {
      case "add":
        this.drawConstraintws(newShape);
        break;
      case "delete":
        this.deleteConstraintws(oldShape);
        break;
      case "change":
        this.changeConstraintws(oldShape, newShape);
        break;
    }
  }
  wsProject(diagramContentChangeEvent: DiagramContentChangeEvent) {
    let pro: Project = diagramContentChangeEvent.newProject;
    let newProject = new Project();
    newProject.initProject(pro);
    this.project.changeMachineWithNewProject(newProject);
    this.project.changeProblemDomainWithNewProject(newProject);
    this.project.changeRequirementWithNewProject(newProject);
    this.project.changeInterfaceWithNewProject(newProject);
    this.project.changeConstraintWithNewProject(newProject);
    this.project.changeReferenceWithNewProject(newProject);
    //画图
    console.log("wsProject")
    this.drawDiagram(this.project);
  }

  //----------发送消息  new -----------
  //向后台发送register请求,根据title打开服务器已保存项目，若有其他编辑器编辑该项目，则以服务器正在编辑的版本为准
  //向后台请求数据
  register_new(title, version: string) {
    version = version ? version.replace(":", "_").replace(":", "_") : "undefined";
    this.projectAddress = title;
    this.version = version;
    this.uri = this.username + title + version;
    let diagram = {
      uri: this.uri,
      username: this.username,
      projectAddress: title,
      projectVersion: version,
    };
    let params = { diagram: diagram };
    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      "Diagram/register",
      params
    );
    this.sendMessage2(this.ws, message);
  }
  //向后台发送project,若无其他editor编辑改项目，则以当前project为准，若有其他编辑器编辑该项目，则以服务器版本为准
  register(title, version: string, pro: Project) {
    if (pro == undefined) pro = this.project
    if (version == undefined) version = "undefined";
    version = version.replace(":", "_").replace(":", "_");
    this.projectAddress = title;
    this.version = version;
    this.uri = this.username + title + version;
    let diagram = {
      uri: this.uri,
      username: this.username,
      project: pro,
    };
    let params = { diagram: diagram };
    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      "Diagram/didOpen",
      params
    );
    console.log(message)
    this.sendMessage2(this.ws, message);
  }
  sendTimeRecoder(lastVersion, T0, T1, T2, T3, T4, Flag1, Flag2, Flag3) {
    // let timeRecoder = {
    //   uri: this.projectAddress + this.version,
    //   lastVersion: lastVersion,
    //   T0: T0,
    //   T1: T1,
    //   T2: T2,
    //   T3: T3,
    //   T4: T4,
    //   Flag1: Flag1,
    //   Flag2: Flag2,
    //   Flag3: Flag3
    // };
    // let message = LSPMessageFactory.getMessageNoFraming(
    //   this.messageId++,
    //   "Diagram/TimeRecoder",
    //   timeRecoder
    // );
    // this.sendMessage2(this.ws, message);
  }

  heartbeatMechanism() {
    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      "Diagram/heartbeatMechanism",
      null
    );
    this.sendMessage2(this.ws, message);
  }

  //没有分包，直接发送message
  sendMessage2(ws, message) {
    let i = 0;
    let that = this;
    let interval1 = setInterval(function () {
      if (ws.readyState == WebSocket.OPEN) {
        clearInterval(interval1);
        //console.log(message);
        ws.send(JSON.stringify(message));
      } else if (ws.readyState == WebSocket.CLOSED) {
        // // console.log("diagram.ws.readyState=CLOSED ", ws.readyState);
      } else if (ws.readyState == WebSocket.CLOSING) {
        // // console.log("diagram.ws.readyState=CLOSING ", ws.readyState);
      } else if (ws.readyState == WebSocket.CONNECTING) {
        // // console.log("diagram.ws.readyState=CONNECTING ", ws.readyState);
      }
    }, 1000);
  }
  unregister() {
    let diagram = {
      uri: this.uri,
    };
    let params = { diagram: diagram };
    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      "Diagram/TimeRecoder",
      params
    );
    this.sendMessage2(this.ws, message);
  }
  sendChangeShapeMessage(
    changeType: string,
    shapeType: string,
    oldShape,
    newShape
  ) {
    let version = this.version;
    if (version == undefined) version = "undefined";
    // // //console.log("this.messageId=",this.messageId)
    let diagram = {
      uri: this.uri,
      version: null,
      lastVersion: this.lastVersion,
      changeTime: new Date().getTime(),
      T0: new Date().getTime(),
      T1: 0,
      T2: 0,
      T3: 0,
      T4: 0,
      Flag1: 0,
      Flag2: 0,
      Flag3: 0,
    };
    let contentChanges = [
      new DiagramContentChangeEventFactory().getShapeChangeEvent(
        shapeType,
        changeType,
        oldShape,
        newShape
      ),
    ];
    let params = {
      diagram: diagram,
      contentChanges: contentChanges,
    };

    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      "Diagram/didChange",
      params
    );
    this.sendMessage2(this.ws, message);
  }
  sendChangePositionMessage() {
    let version = this.version;
    if (version == undefined) version = "undefined";
    // // //console.log("this.messageId=",this.messageId)
    let params = {
      uri: this.uri,
      moveTimes: this.moveTimes,
    };

    let message = LSPMessageFactory.getMessageNoFraming(
      this.messageId++,
      "Diagram/moveTimes",
      params
    );
    this.sendMessage2(this.ws, message);
  }
  //========================================编号========================================
  searchMaxPheNo(project) {
    let no = this.searchMaxPheNo1(0, project.contextDiagram.interfaceList);
    no = this.searchMaxPheNo1(no, project.problemDiagram.referenceList);
    no = this.searchMaxPheNo1(no, project.problemDiagram.constraintList);
    // // //console.log('searchMaxPheNo1');
    //console.log(no)
    return no;
  }
  searchMaxPheNo1(no, linkList) {
    for (let link of linkList) {
      for (let phe of link.phenomenonList) {
        if (phe.no > no) {
          //  //console.log(phe.no,">",no)
          no = phe.no;
        }
      }
    }
    return no;
  }
  searchMaxLinkNameNo(project) {
    //console.log(project);
    let name = "a";
    if (project.problemDiagram != undefined) {
      for (let reference of project.problemDiagram.referenceList) {
        name = this.compareLinkName(name, reference.name);
      }
      for (let constraint of project.problemDiagram.constraintList) {
        name = this.compareLinkName(name, constraint.name);
      }
    }
    for (let my_interface of project.contextDiagram.interfaceList) {
      name = this.compareLinkName(name, my_interface.name);
      //console.log(name+'<'+my_interface.name);
    }
    //console.log(name);
    return this.getLinkNameNo(name);
  }
  compareLinkName(n1, n2) {
    if (n2 == null) return n1;
    if (n1 == null) return n2;
    for (let i of n2) {
      if (i < "a" && i > "z") {
        return n1;
      }
    }
    if (n1.length > n2.length) {
      return n1;
    } else if (n1.length < n2.length) {
      return n2;
    } else if (n1 < n2) {
      return n2;
    } else return n1;
  }
  getlink_name(link_name_no) {
    let a = link_name_no % 26;
    let b = Math.floor(link_name_no / 26);
    if (a == 0) {
      a = 26;
      b -= 1;
    }
    let res = String.fromCharCode(96 + a);
    while (b > 0) {
      a = b % 26;
      b = Math.floor(b / 26);
      if (a == 0) {
        a = 26;
        b -= 1;
      }
      let temp = String.fromCharCode(96 + a);
      res = temp + res;
    }
    return res;
  }
  getLinkNameNo(name: string) {
    let no = 0;
    for (let i = 0; i < name.length; i++) {
      no *= 26;
      no = no + name.charCodeAt(i) - 96;
    }
    return no;
  }
  getShortname(name) {
    let len = name.length;
    let flag = true;
    let res = "";
    for (let i = 0; i < len; i++) {
      if (
        flag &&
        ((name.charAt(i) > "a" && name.charAt(i) < "z") ||
          (name.charAt(i) > "A" && name.charAt(i) < "Z"))
      ) {
        res += name.charAt(i);
        //console.log(res)
        flag = false;
      }
      if (name.charAt(i) == "_" || name.charAt(i) == " ") {
        // // //console.log('flag=true')
        flag = true;
      }
    }
    //console.log(name,res.toUpperCase())
    return res.toUpperCase();
  }
  isPop: boolean = false;


  setProject(project) {
    this.project.initProject(project);
    this.link_name_no = this.searchMaxLinkNameNo(this.project) + 1;
    this.phenomenon_no = this.searchMaxPheNo(this.project) + 1;
    this.dealConstraint(this.project);
  }
  //打开项目后第二次接受到registered消息调用，保存原有位置消息
  changeProject(project) {
    this.project.initProject(project);
    this.link_name_no = this.searchMaxLinkNameNo(this.project) + 1;
    this.phenomenon_no = this.searchMaxPheNo(this.project) + 1;
    this.dealConstraint(this.project);
  }

  //處理 reference 和 constraint上的constraint標記
  dealConstraint(project) {
    for (let temp of project.problemDiagram.referenceList) {
      for (let phe of temp.phenomenonList) {
        if (phe.constraint == null) {
          // // //console.log('null-------')
          phe.constraint = false;
        }
      }
    }
    for (let temp of project.problemDiagram.constraintList) {
      for (let phe of temp.phenomenonList) {
        if (phe.constraint == null) {
          // // //console.log('null-------')
          phe.constraint = false;
        }
      }
    }
  }
  //-----------------画图----------------------------
  initPapers() { }
  //绘制图形
  drawDiagram(project: Project) {
    console.log("drawDiagram")
    this.drawProblemDiagram(project.problemDiagram);
  }

  //ProblemDiagram
  drawProblemDiagram(problemDiagram: ProblemDiagram) {
    var window
    let root = window.graph.getDefaultParent();
    window.graph.removeCells(root.children, true);
    if (problemDiagram.contextDiagram.machine != null)
      this.drawMachineOnGraph(problemDiagram.contextDiagram.machine);
    this.drawProblemDomains(problemDiagram.contextDiagram.problemDomainList);
    this.drawInterfaces(problemDiagram.contextDiagram.interfaceList);
    this.drawRequirements(problemDiagram.requirementList);
    this.drawConstraints(problemDiagram.constraintList);
    this.drawReferences(problemDiagram.referenceList);
  }

  //Cell
  deleteElement() {
    // if (this.selectedType == "machine") {
    //   this.deleteMachine();
    // } else if (this.selectedType == "problemDomain") {
    //   this.deleteProblemDomain();
    // } else if (this.selectedType == "requirement") {
    //   this.deleteRequirement();
    // } else if (this.selectedType == "interface") {
    //   // // //console.log('deleteElement');
    //   this.deleteInterface();
    // } else if (this.selectedType == "constraint") {
    //   this.deleteConstraint();
    // } else if (this.selectedType == "reference") {
    //   this.deleteReference();
    // } 
  }

  //========================================Shape======================================
  drawNodeOnGraphWithPostMessage(type, name, shortname, x, y) {
    this.postMessage()
    this.drawNodeOnGraph(type, name, shortname, x, y)
  }

  drawNodeOnGraph(type, name, shortname, x, y) {
    var window
    var graph = window.graph;
    let root = window.graph.getDefaultParent();
    var cell
    switch (type) {
      case 'machine':
        cell = graph.insertVertex(root, null, '', x, y, 120, 60,
          'shape=machine;whiteSpace=wrap;html=1;backgroundOutline=1;')
        break;
      case 'problemdomain':
        cell = graph.insertVertex(root, null, '', x, y, 120, 60,
          'shape=domain;rounded=0;whiteSpace=wrap;html=1;')
        break;
      case 'requirement':
        cell = graph.insertVertex(root, null, '', x, y, 120, 60,
          'shape=ellipse;dashed=1;whiteSpace=wrap;html=1;')
        break;
      default:
        break;
    }
    cell.name = name;
    cell.shortname = shortname;
    cell.shape = type;
    cell.value = name + "\n(" + shortname + ")";
    cell.setAttribute('label', cell.value);
    graph.cellLabelChanged(cell, cell.value)
  }
  changeNodeOnGraph(oldName, name, shortname) {
    this.postMessage()
    var window
    if (window.cells == undefined) {
      console.log("window.cells == undefined")
      return;
    }

    let root = window.graph.getDefaultParent();
    for (let cell of root.children) {
      if (cell.name === oldName) {
        cell.value = name + "\n(" + shortname + ")";
        cell.name = name;
        cell.shortname = shortname;
        cell.setAttribute('label', cell.value);
        window.graph.cellLabelChanged(cell, cell.value)
        break;
      }
    }
  }
  changeNodePositionWithPostMessage(name, x, y) {
    this.changeNodePosition(name, x, y)
    this.postMessage()
  }
  changeNodePosition(name, x, y) {
    if (this.project.changeMachinePosition(name, x, y)) return true;
    if (this.project.changeProblemDomainPosition(name, x, y)) return true;
    if (this.project.changeRequirementPosition(name, x, y)) return true;
    return false;
  }
  deleteShapeOnGraphWithPostMessage(name) {
    this.postMessage()
    var window
    var graph = window.graph;
    if (graph != undefined) {
      let root = graph.getDefaultParent();
      for (let cell of root.children) {
        if (cell.name == name) {
          graph.removeCells([cell], true);
        }
      }
    }

  }

  // 根据类型及名字设置当前选中节点
  setSelectedShape(type, name) {
    this.selectedType = type
    if (this.selectedType == "machine") {
      this.machine = this.project.contextDiagram.machine;
    } else if (this.selectedType == "problemdomain") {
      this.problemDomain = this.selectShape(name, this.project.contextDiagram.problemDomainList);
    } else if (this.selectedType == "requirement") {
      this.requirement = this.selectShape(name, this.project.problemDiagram.requirementList);
    } else if (this.selectedType == "interface") {
      this.interface = this.selectShape(name, this.project.contextDiagram.interfaceList);
      if (this.interface)
        this.phenomenonList = this.interface.phenomenonList
    } else if (this.selectedType == "constraint") {
      this.constraint = this.selectShape(name, this.project.problemDiagram.constraintList);
      if (this.constraint)
        this.requirementPhenomenonList = this.constraint.phenomenonList
    } else if (this.selectedType == "reference") {
      this.reference = this.selectShape(name, this.project.problemDiagram.referenceList);
      if (this.reference)
        this.requirementPhenomenonList = this.reference.phenomenonList
    }
  }
  selectShape(name, list) {
    for (let shape of list) {
      if (shape.name == name)
        return shape
    }
    return null
  }

  // ========================================Machine========================================
  //初始化时画图，只画图，不修改project
  drawMachineOnGraph(wsmachine: Machine) {
    let machine = MachineOperation.copyShape(wsmachine);
    if (
      machine.getName() == "undefined" ||
      machine.getName() == undefined ||
      machine.getShortname() == "undefined" ||
      machine.getShortname() == undefined
    )
      return;
    this.drawNodeOnGraph("machine", machine.name, machine.shortname, machine.x, machine.y)
  }//根据ws消息画图并修改project
  drawMachinews(wsmachine: Machine) {
    let machine = MachineOperation.copyShape(wsmachine);
    if (
      machine.getName() == "undefined" ||
      machine.getName() == undefined ||
      machine.getShortname() == "undefined" ||
      machine.getShortname() == undefined
    )
      return;
    this.project.problemDiagram.contextDiagram.machine = machine;
    this.project.contextDiagram.machine = machine;
    this.drawNodeOnGraphWithPostMessage("machine", machine.name, machine.shortname, machine.x, machine.y)
  }
  //鼠标点击画板，仅向服务器发送add_machine消息
  drawMachine(x, y) {
    if (this.project.contextDiagram.machine == undefined) {
      let machine = MachineOperation.newShape("machine", "M1", x, y, 100, 50);
      this.sendChangeShapeMessage("add", "mac", null, machine);
    } else {
      console.log("machine already exist!");
    }
  }
  //修改Machine信息，仅向服务器发送消息
  changeMachineDetail(description, shortname) {
    if (this.isKeyWord(shortname)) return false;
    let old = this.project.contextDiagram.machine;
    let machine = MachineOperation.newShapeWithOld(old, description, shortname);
    this.sendChangeShapeMessage("change", "mac", old, machine);
    return true;
  }
  //根据服务器传来的消息修改Machine(连线，图上Machine以及Project中的machine)
  changeMachinews(old: Machine, new1: Machine) {
    this.changeRelatedLink(old.shortname, new1.shortname);
    this.changeNodeOnGraph(old.name, new1.name, new1.shortname);
    this.project.changeMachine(new1.name, new1.shortname);
    return true;
  }
  //向服务器发送删除消息
  deleteMachine(name) {
    let old = this.project.contextDiagram.machine;
    if (old == undefined) {
      // oldPd = ProblemDomainOperation.newShapeWithName(name)
      this.deleteShapeOnGraphWithPostMessage(name)
    } else {
      this.sendChangeShapeMessage("delete", "mac", old, null);
    }
  }
  //根据ws消息删除project中的machine
  deleteMachinews(old: Machine) {
    old = MachineOperation.copyShape(old);
    let shortname = old.getShortname();
    this.project.deleteRelatedLink(shortname);
    this.project.contextDiagram.machine = undefined;
    this.project.problemDiagram.contextDiagram.machine = undefined;
    this.deleteShapeOnGraphWithPostMessage(old.name)
  }

  //========================================Domain========================================
  //根据problemDomainList在图上画Domains
  drawProblemDomains(
    problemDomainList: ProblemDomain[]
  ) {
    if (problemDomainList.length > 0)
      for (let problemDomain of problemDomainList) {
        this.drawNodeOnGraph("problemdomain", problemDomain.name, problemDomain.shortname, problemDomain.x, problemDomain.y)
      }
  }
  //根据用户操作发送ws消息给server
  drawProblemDomain(x, y) {// to change
    let no, name, shortname;
    while (true) {
      no = this.problemdomain_no;
      this.problemdomain_no += 1;
      name = "problemDomain" + no;
      shortname = "PD" + no;
      let conflicting_name = false;
      for (let pdi of this.project.contextDiagram.problemDomainList) {
        if (pdi.name == name || pdi.shortname == shortname) {
          conflicting_name = true;
        }
      }
      if (!conflicting_name) {
        break;
      }
    }
    let pd = ProblemDomainOperation.newShape(
      no,
      name,
      shortname,
      "Causal",
      "GivenDomain",
      x,
      y,
      100,
      50
    );
    this.sendChangeShapeMessage("add", "pro", null, pd);
  }
  //根据ws消息在project上添加pd
  drawProblemDomainws(problemDomain: ProblemDomain) {
    problemDomain = ProblemDomainOperation.copyShape(problemDomain);
    this.project.contextDiagram.problemDomainList.push(problemDomain);
    this.drawNodeOnGraphWithPostMessage("problemdomain", problemDomain.name, problemDomain.shortname, problemDomain.x, problemDomain.y)
  }
  //发送change_pd消息
  changeProblemDomainDetail(description, shortname, physicalProperty, domainType) {
    let old = this.problemDomain;
    let pd = ProblemDomainOperation.newShapeWithOld(
      old,
      description,
      shortname,
      domainType,
      physicalProperty
    );
    this.sendChangeShapeMessage("change", "pro", old, pd);
    return true;
  }
  //根据ws消息修改project上的pd信息,并修改图上信息
  changeProblemDomainws(old: ProblemDomain, new1: ProblemDomain) {
    old = ProblemDomainOperation.copyShape(old);
    new1 = ProblemDomainOperation.copyShape(new1);
    this.changeRelatedLink(old.shortname, new1.shortname);
    this.project.changeProblemDomain1(old, new1);
    this.changeNodeOnGraph(old.name, new1.name, new1.shortname)
    return true;
  }
  //发送删除pd的消息
  deleteProblemDomain(name) {
    let pdList = this.project.contextDiagram.problemDomainList;
    //delete problem domain
    let i = 0;
    let oldPd;
    for (let item of pdList) {
      if (item.name == name) {
        oldPd = item;
        break;
      }
      i++;
    }
    if (oldPd == undefined) {
      // oldPd = ProblemDomainOperation.newShapeWithName(name)
      this.deleteShapeOnGraphWithPostMessage(name)
    } else {
      this.sendChangeShapeMessage("delete", "pro", oldPd, null);
    }
  }
  //根据ws消息删除project中的pd
  deleteProblemDomainws(pd1: ProblemDomain) {
    let pd = ProblemDomainOperation.copyShape(pd1);
    let name = pd.name;
    let shortname = pd.shortname;
    let pdList = this.project.contextDiagram.problemDomainList;

    this.project.deleteRelatedLink(shortname);
    let i = 0;
    for (let item of pdList) {
      if (item.name == name) {
        pdList.splice(i, 1);
        // // console.log("delete pd", shortname);
        break;
      }
      i++;
    }
    this.deleteShapeOnGraphWithPostMessage(name)
  }
  //========================================Requirement========================================
  drawRequirements(
    requirementList: Requirement[]
  ) {
    for (let requirement of requirementList) {
      this.drawNodeOnGraph("requirement", requirement.name, requirement.shortname, requirement.x, requirement.y)
    }
  }
  //用户点击后，向server发送add_Req消息
  drawRequirement(x, y) {
    let no;
    let name;
    while (true) {
      no = this.requirement_no;
      this.requirement_no += 1;
      name = "requirement" + no;
      let conflicting_name = false;
      for (let reqi of this.project.problemDiagram.requirementList) {
        if (reqi.name == name) {
          conflicting_name = true;
        }
      }
      if (!conflicting_name) {
        break;
      }
    }
    let req = RequirementOperation.newShape(no, name, x, y, 100, 50);
    this.sendChangeShapeMessage("add", "req", null, req);
  }
  //根据ws消息在project上添加pd并画图
  drawRequirementws(req1) {
    let requirement = RequirementOperation.copyShape(req1);
    this.project.problemDiagram.requirementList.push(requirement);
    this.drawNodeOnGraphWithPostMessage("requirement", requirement.name, requirement.shortname, requirement.x, requirement.y)
  }
  //用户修改后，向Server发送消息
  changeRequirementDetail(description, shortname) {
    if (this.isKeyWord(shortname)) return false;
    for (let existedRq of this.project.problemDiagram.requirementList) {
      if (
        (existedRq as any).name == description &&
        existedRq.name != this.requirement.name
      ) {
        console.log(description + "already exist!");
        return false;
      }
      if (
        (existedRq as any).shortname == shortname &&
        existedRq.shortname != this.requirement.shortname
      ) {
        console.log(shortname + "already exist!");
        return false;
      }
    }
    let old = this.requirement;
    let newR = RequirementOperation.newShapeWithOld(old, description, shortname);
    this.sendChangeShapeMessage("change", "req", old, newR);
    return true;
  }
  //根据ws消息在project上修改req
  changeRequirementws(old1: Requirement, new2: Requirement) {
    let old = RequirementOperation.copyShape(old1);
    let new1 = RequirementOperation.copyShape(new2);
    this.changeRelatedLink(old.name, new1.name);
    this.project.changeRequirement1(old, new1);
    this.changeNodeOnGraph(old.name, new1.name, new1.shortname)
    return true;
  }
  //向服务器发生删除信息
  deleteRequirement(name) {
    let list = this.project.problemDiagram.requirementList;
    let i = list.length - 1;
    let old;
    for (; i >= 0; i--) {
      let item = list[i];
      if (item.name == name) {
        old = item;
        break;
      }
    }
    if (old == undefined) this.deleteShapeOnGraphWithPostMessage(name)
    else this.sendChangeShapeMessage("delete", "req", old, null);
  }
  //根据server传来的消息修改project
  deleteRequirementws(requirement: Requirement) {
    let req = RequirementOperation.copyShape(requirement);
    this.project.deleteRequirement(req);
    this.project.deleteRelatedLink(req.getName());
    this.deleteShapeOnGraphWithPostMessage(req.name)
  }

  //========================================Link========================================
  changeRelatedLink(oldName, newName) {
    let i = this.project.problemDiagram.referenceList.length - 1;
    for (let reference of this.project.problemDiagram.referenceList) {
      if (reference.from == oldName) {
        reference.from = newName;
      } else if (reference.to == oldName) {
        reference.to = newName;
      }
      reference.refreshPhenomenonList(oldName, newName);
    }
    for (let constraint of this.project.problemDiagram.constraintList) {
      if (constraint.from == oldName) {
        constraint.from = newName;
      } else if (constraint.to == oldName) {
        constraint.to = newName;
      }
      constraint.refreshPhenomenonList(oldName, newName);
    }
    for (let my_interface of this.project.contextDiagram.interfaceList) {
      if (my_interface.from == oldName) {
        my_interface.from = newName;
      } else if (my_interface.to == oldName) {
        my_interface.to = newName;
      }
      my_interface.refreshPhenomenonList(oldName, newName);
    }
  }
  getReqNo(req_context) {
    for (const req of this.project.problemDiagram.requirementList) {
      if (req.name == req_context) {
        return req.no;
      }
    }
    return -1;
  }
  getLinkName(): string {
    while (true) {
      let name = this.getlink_name(this.link_name_no);
      this.link_name_no += 1;
      let conflicting_name = false;
      for (let inti of this.project.contextDiagram.interfaceList) {
        if (inti.name == name) {
          conflicting_name = true;
        }
      }
      for (let refi of this.project.problemDiagram.referenceList) {
        if (refi.name == name) {
          conflicting_name = true;
        }
      }
      for (let coni of this.project.problemDiagram.constraintList) {
        if (coni.name == name) {
          conflicting_name = true;
        }
      }
      if (!conflicting_name) {
        return name;
      }
    }
  }
  // 根据类型及起始点设置当前选中连线及现象列表
  setSelectedLink(type, from, to) {
    this.selectedType = type
    if (this.selectedType == "interface") {
      this.interface = this.selectLink(from, to, this.project.contextDiagram.interfaceList);
      this.phenomenonList = this.interface.phenomenonList
    } else if (this.selectedType == "constraint") {
      this.constraint = this.selectLink(from, to, this.project.problemDiagram.constraintList);
      this.requirementPhenomenonList = this.constraint.phenomenonList
    } else if (this.selectedType == "reference") {
      this.reference = this.selectLink(from, to, this.project.problemDiagram.referenceList);
      this.requirementPhenomenonList = this.reference.phenomenonList
    }
  }
  selectLink(from, to, list) {
    for (let shape of list) {
      if (shape.from === from && shape.to === to || shape.from === to && shape.to === from)
        return shape
    }
    return null
  }
  changeLinkPosition(project) {
    if (project == undefined || project == null) return;
    if (project.problemDiagram == undefined || project.problemDiagram == null)
      return;
    if (
      project.problemDiagram.referenceList != undefined ||
      project.problemDiagram.referenceList != null
    )
      for (let reference of project.problemDiagram.referenceList) {
        let pos = this.changeLinkPosition1(
          reference.from,
          reference.to,
          project
        );
        reference.x1 = pos[0];
        reference.x2 = pos[1];
        reference.y1 = pos[2];
        reference.y2 = pos[3];
      }
    if (
      project.problemDiagram.constraintList != undefined ||
      project.problemDiagram.constraintList != null
    )
      for (let constraint of project.problemDiagram.constraintList) {
        let pos = this.changeLinkPosition1(
          constraint.from,
          constraint.to,
          project
        );
        constraint.x1 = pos[0];
        constraint.x2 = pos[1];
        constraint.y1 = pos[2];
        constraint.y2 = pos[3];
      }
    if (project.contextDiagram == undefined || project.contextDiagram == null)
      return;
    if (
      project.contextDiagram.interfaceList != undefined ||
      project.contextDiagram.interfaceList != null
    )
      for (let my_interface of project.contextDiagram.interfaceList) {
        let pos = this.changeLinkPosition1(
          my_interface.from,
          my_interface.to,
          project
        );
        my_interface.x1 = pos[0];
        my_interface.x2 = pos[1];
        my_interface.y1 = pos[2];
        my_interface.y2 = pos[3];
      }
  }
  changeLinkPosition1(from, to, project) {
    let x1, x2, y1, y2;
    if (from == project.contextDiagram.machine.shortname) {
      x1 = this.project.contextDiagram.machine.x;
      y1 = this.project.contextDiagram.machine.y;
    } else if (to == this.project.contextDiagram.machine.shortname) {
      x2 = this.project.contextDiagram.machine.x;
      y2 = this.project.contextDiagram.machine.y;
    }

    for (let ele of project.contextDiagram.problemDomainList) {
      if (ele.shortname == from) {
        x1 = ele.x;
        y1 = ele.y;
      } else if (ele.shortname == to) {
        x2 = ele.x;
        y2 = ele.y;
      }
    }

    for (let ele of project.problemDiagram.requirementList) {
      if (ele.name == from) {
        x1 = ele.x;
        y1 = ele.y;
      } else if (ele.name == to) {
        x2 = ele.x;
        y2 = ele.y;
      }
    }
    return [x1, x2, y1, y2];
  }
  deleteRelatedLinkOnGraph(name) {
    var window
    var graph = window.graph;
    let root = graph.getDefaultParent();
    for (let cell of root.children) {
      if (cell.name == name) {
        graph.removeCells([cell], true);
      }
    }
  }
  //在图上修改线的name
  changeLinkOnGraph(type, link) {
    this.postMessage()
    var from = link.from
    var to = link.to
    var window
    let root = window.graph.getDefaultParent();
    let isAdd = false;
    for (let cell of root.children) {
      if (cell.source != null && cell.target != null &&
        (cell.source.shortname == from && cell.target.shortname == to ||
          cell.source.shortname == to && cell.target.shortname == from)) {
        cell.value = link.name
        cell.name = link.name
        cell.setAttribute('label', link.name);
        window.graph.cellLabelChanged(cell, link.name)
        isAdd = true;
      }
    }
    if (!isAdd) {
      this.drawLinkOnGraph(type, link)
    }
  }
  //在图上画线
  drawLinkOnGraph(type, link) {
    this.postMessage()
    var from = link.from
    var to = link.to
    var window
    let root = window.graph.getDefaultParent();
    let source, target;
    for (let cell of root.children) {
      if (cell.shortname == from) {
        source = cell
        if (target) break
      } else if (cell.shortname == to) {
        target = cell
        if (source) break
      }
    }
    var cell
    switch (type) {
      case 'interface':
        cell = window.graph.insertEdge(root, null, '', source, target, 'shape=interface;endArrow=none;html=1;');
        break;
      case 'reference':
        cell = window.graph.insertEdge(root, null, '', source, target, 'shape=reference;endArrow=none;dashed=1;html=1;dashPattern=1 3;strokeWidth=2;');
        break;
      case 'constraint':
        cell = window.graph.insertEdge(root, null, '', source, target, 'shape=constraint;endArrow=classic;dashed=1;html=1;');
        break;
      default:
        break;
    }
    cell.name = link.name
    cell.shape = type;
    cell.value = link.name
    cell.setAttribute('label', link.name);
    window.graph.cellLabelChanged(cell, link.name)
  }
  //若能找到link则返回，否则从图中删除link，并返回null
  deleteLink(from, to, list) {
    this.postMessage()
    let link;
    for (let item of list) {
      if (item.from == from && item.to == to || item.from == to && item.to == from) {
        link = item;
        break;
      }
    }
    if (link == null)
      this.deleteLinkOnGraph(from, to)
    return link
  }
  deleteLinkOnGraph(from, to) {
    this.postMessage()
    var window
    let root = window.graph.getDefaultParent();
    for (let cell of root.children) {
      if (cell.source.name == from && cell.target.name == to ||
        cell.source.name == to && cell.target.name == from) {
        window.graph.removeCells([cell], true)
      }
    }
  }
  // ========================================Interface========================================
  drawInterfaces(interfaceList: Interface[]) {
    for (const inte of interfaceList) {
      this.drawLinkOnGraph("interface", inte);
    }
  }
  //发送ws消息
  drawInterface(from, to) {
    let no;
    while (true) {
      no = this.interface_no;
      this.interface_no += 1;
      let conflicting_no = false;
      for (let inti of this.project.contextDiagram.interfaceList) {
        if (inti.no == no) {
          conflicting_no = true;
        }
      }
      if (!conflicting_no) {
        break;
      }
    }
    let name = this.getLinkName();
    let myinterface = InterfaceOperation.newShape(
      no,
      name,
      name + "?",
      from,
      to,
      [],
      0,
      0,
      0,
      0
    );
    this.sendChangeShapeMessage("add", "int", null, myinterface);
    return myinterface;
  }
  //根据ws消息修改project，给interface添加名称
  drawInterfacews(int1: Interface) {
    let int = InterfaceOperation.copyShape(int1);
    this.project.addInterface(int);
    this.changeLinkOnGraph("interface", int);
  }

  getProblemEntityByShortName(shortname) {
    for (let pro of this.project.contextDiagram.problemDomainList) {
      if (pro.shortname == shortname) return pro;
    }
    return null;
  }
  changeInterfaceDetail() { }
  changeInterfacews(old: Interface, new1: Interface) {
    old = InterfaceOperation.copyShape(old);
    new1 = InterfaceOperation.copyShape(new1);
    this.project.setDescription(new1);
    this.project.changeInterface(old, new1);
    this.changeLinkOnGraph("interface", new1);
    return true;
  }

  //向服务器发送删除接口的信息
  deleteInterface(from, to) {
    let list = this.project.contextDiagram.interfaceList;
    let inter = this.deleteLink(from, to, list)
    if (inter != null)
      this.sendChangeShapeMessage("delete", "int", inter, null);
  }
  //根据ws信息删除interface
  deleteInterfacews(int: Interface) {
    int = InterfaceOperation.copyShape(int);
    this.project.deleteInterface(int);
    this.deleteLinkOnGraph(int.from, int.to);
  }

  //========================================Reference========================================
  drawReferences(
    referenceList: Reference[]
  ) {
    for (const ref of referenceList) {
      this.drawLinkOnGraph("reference", ref);
    }

  }
  //发送ws消息
  drawReference(from, to) {
    let no;
    while (true) {
      no = this.reference_no;
      this.reference_no += 1;
      let conflicting_no = false;
      for (let link of this.project.problemDiagram.referenceList) {
        if (link.no == no) {
          conflicting_no = true;
        }
      }
      if (!conflicting_no) {
        break;
      }
    }
    let name = this.getLinkName();
    let ref = ReferenceOperation.newShape(
      no,
      name,
      name + "?",
      from,
      to,
      [],
      0,
      0,
      0,
      0
    );
    this.sendChangeShapeMessage("add", "ref", null, ref);
    return ref;
  }
  //根据ws消息修改project并画reference（或者修改reference.name）
  drawReferencews(ref) {
    ref = ReferenceOperation.copyShape(ref);
    this.project.addReference(ref);
    this.changeLinkOnGraph("reference", ref);
  }
  getInitiator(from, to) {
    for (let temp of this.project.contextDiagram.problemDomainList) {
      if (
        temp.shortname == from ||
        temp.shortname == to
      ) {
        return temp.shortname;
      }
    }
    return null
  }

  exist(phe, pheList) {
    for (let temp of pheList) {
      if (phe.name == temp.name) {
        return true;
      }
    }
    return false;
  }
  getRelatedPhes(proName, phes, unRelatedPhes) {
    for (let item of this.project.contextDiagram.interfaceList) {
      this.getRelatedPhes1(proName, item.phenomenonList, phes, unRelatedPhes);
    }
    for (let item of this.project.problemDiagram.constraintList) {
      this.getRelatedPhes1(proName, item.phenomenonList, phes, unRelatedPhes);
    }
    for (let item of this.project.problemDiagram.referenceList) {
      this.getRelatedPhes1(proName, item.phenomenonList, phes, unRelatedPhes);
    }
  }
  getRelatedPhes1(name, pheList, phes, unRelatedPhes) {
    for (let item of pheList) {
      if (name == item.from || name == item.to) {
        phes.push(item);
      } else {
        unRelatedPhes.push(item);
      }
    }
  }
  changereceiver() {
    // to do
  }
  changeReferenceDetail() { }
  //根据ws消息修改ref
  changeReferencews(old: Reference, new1: Reference) {
    old = ReferenceOperation.copyShape(old);
    new1 = ReferenceOperation.copyShape(new1);
    this.project.setDescription(new1);
    this.project.changeReference(old, new1);
    this.changeLinkOnGraph("reference", new1);
    return true;
  }
  deleteReference(from, to) {
    let list = this.project.problemDiagram.referenceList;
    let ref = this.deleteLink(from, to, list)
    if (ref != null) this.sendChangeShapeMessage("delete", "ref", ref, null);
  }
  deleteReferencews(ref: Reference) {
    ref = ReferenceOperation.copyShape(ref);
    this.project.deleteReference(ref);
    this.deleteLinkOnGraph(ref.from, ref.to);
  }

  // //========================================Constraint========================================
  drawConstraints(constraintList) {
    for (const con of constraintList) {
      this.drawLinkOnGraph("constraint", con);
    }
  }
  drawConstraint(from, to) {
    let name = this.getLinkName();
    let no;
    while (true) {
      no = this.constraint_no;
      this.constraint_no += 1;
      let conflicting_no = false;
      for (let coni of this.project.problemDiagram.constraintList) {
        if (coni.no == no) {
          conflicting_no = true;
        }
      }
      if (!conflicting_no) {
        break;
      }
    }
    let req: string;
    //确保箭头（target）放在domain这一侧
    //to do
    let con = ConstraintOperation.newShape(no, name, "", from, to, [], 0, 0, 0, 0);
    this.sendChangeShapeMessage("add", "con", null, con);
  }
  drawConstraintws(con) {
    con = ConstraintOperation.copyShape(con);
    this.project.addConstraint(con);
    this.changeLinkOnGraph("constraint", con);
  }

  initConstraintPopBox(name) {
    for (let item of this.project.problemDiagram.constraintList) {
      if (item.name == name) {
        this.constraint = item;
        this.requirementPhenomenonList = item.phenomenonList;
        break;
      }
    }

    //initiator and receiverList
    this.receiverList = []; //link to initiator

    //get initiator
    for (let temp of this.project.contextDiagram.problemDomainList) {
      if (
        temp.shortname == this.constraint.from ||
        temp.shortname == this.constraint.to
      ) {
        //get initiator
        this.initiator = temp.shortname;
      }
    }

    //get receiver list
    this.getReceiverList()

    //form interface
    this.interfacePhes = [];

    //get reference phenomenon list according to ontology
    let pro = this.getProblemEntityByShortName(this.initiator);
    this.ontologyPhes = pro.phes;

  }
  getReceiverList() {
    this.receiverList = []
    //get receiver list
    for (let temp_int of this.project.contextDiagram.interfaceList) {
      if (temp_int.from == this.initiator) {
        this.receiverList.push(temp_int.to);
      } else if (temp_int.to == this.initiator) {
        this.receiverList.push(temp_int.from);
      }
    }
    return this.receiverList
  }

  changeConstraintDetail() { }
  changeConstraintws(old: Constraint, new1: Constraint) {
    old = ConstraintOperation.copyShape(old);
    new1 = ConstraintOperation.copyShape(new1);
    this.project.setDescription(new1);
    this.project.changeConstraint(old, new1);
    this.changeLinkOnGraph("constraint", new1);
    return true;
  }
  deleteConstraint(from, to) {
    let list = this.project.problemDiagram.constraintList;
    let con = this.deleteLink(from, to, list)
    if (con)
      this.sendChangeShapeMessage("delete", "con", con, null);
  }
  deleteConstraintws(con: Constraint) {
    con = ConstraintOperation.copyShape(con);
    this.project.deleteConstraint(con);
    this.deleteLinkOnGraph(con.from, con.to);
  }
  //============================phenomenon===================
  //get phenomenon list of interface
  getPhenomenonList(shortname) {
    for (let int of this.project.contextDiagram.interfaceList) {
      if (int.from == shortname || int.to == shortname) {
        return int.phenomenonList;
      }
    }
    return null;
  }
  //get phenomenon list of link according to problem shortname
  getRefPheList(shortname) {
    let res = [];
    for (let int of this.project.contextDiagram.interfaceList) {
      if (int.from == shortname || int.to == shortname) {
        for (var i = 0; i < int.phenomenonList.length; i++) {
          res.push(int.phenomenonList[i]);
        }
      }
    }
    for (let item of this.project.problemDiagram.referenceList) {
      if (item.from == shortname || item.to == shortname) {
        for (var i = 0; i < item.phenomenonList.length; i++) {
          res.push(item.phenomenonList[i]);
        }
      }
    }
    for (let item of this.project.problemDiagram.constraintList) {
      if (item.from == shortname || item.to == shortname) {
        for (var i = 0; i < item.phenomenonList.length; i++) {
          res.push(item.phenomenonList[i]);
        }
      }
    }
    for (let i = 0; i < res.length; i++) {
      for (let j = res.length - 1; j > i; j--) {
        if (res[i].no == res[j].no) {
          res.splice(j, 1);
        }
      }
    }
    //console.log(res);
    return res;
  }
  //addInterfacePhenomenon
  addInterfacePhenomenon(name, type, from, to) {
    if (this.isKeyWord(name)) return false;
    for (let existPhenomenon of this.phenomenonList) {
      if (existPhenomenon.name == name) {
        console.log(name + "already exists");
        return;
      }
    }
    let phenomenon = PhenomenonOperation.newPhenomenon(this.phenomenon_no++, name, type, from, to)
    this.phenomenonList.push(phenomenon);
    this.project.setDescription(this.interface);
    this.sendChangeShapeMessage(
      "change",
      "int",
      this.interface,
      this.interface
    );
  }
  //addReferencePhenomenon
  addReferencePhenomenon(name, type, from, to) {
    let reqname
    if (this.reference.from === from || this.reference.from == to)
      reqname = this.reference.to
    else if (this.reference.to === from || this.reference.to == to)
      reqname = this.reference.from
    this.addReferencePhenomenon1(reqname, name, type, from, to)
  }
  addReferencePhenomenon1(reqname, name, type, from, to) {
    let phe = this.addRequirementPhenomenon(reqname, name, type, from, to)
    if (phe == null) return
    this.project.setDescription(this.reference);
    this.sendChangeShapeMessage(
      "change",
      "ref",
      this.reference,
      this.reference
    );

  }
  // addConstraintPhenomenon
  addConstraintPhenomenon(name, type, from, to) {
    let reqname
    if (this.constraint.from === from || this.constraint.from == to)
      reqname = this.constraint.to
    else if (this.constraint.to === from || this.constraint.to == to)
      reqname = this.constraint.from
    this.addConstraintPhenomenon1(reqname, name, type, from, to)
  }
  addConstraintPhenomenon1(reqname, name, type, from, to) {
    let phe = this.addRequirementPhenomenon(reqname, name, type, from, to)
    if (phe == null) return
    this.project.setDescription(this.constraint);
    this.sendChangeShapeMessage(
      "change",
      "con",
      this.constraint,
      this.constraint
    );
  }
  // addRequirementPhenomenon used by addReferencePhenomenon1 and addConstraintPhenomenon1
  addRequirementPhenomenon(reqname, name, type, from, to) {
    let reqno = this.getReqNo(reqname);
    let phenomenon
    if (name == "" || name == null || name == undefined)
      return null;
    if (this.isKeyWord(name)) return null;
    let relatedPhes = [];
    let unRelatedPhes = [];
    this.getRelatedPhes(this.initiator, relatedPhes, unRelatedPhes);
    for (let existPhenomenon of this.requirementPhenomenonList) {
      if (existPhenomenon.name == name) {
        console.log(name + "already exists");
        return null;
      }
    }

    //phe exist in related link
    let flag = false;
    for (let phe of relatedPhes) {
      if (phe.name == name) {
        phenomenon = RequirementPhenomenonOperation.copyPhenomenon(phe)
        phenomenon.requirement = reqno;
        flag = true;
      }
    }

    //phe doesn't exist in related link
    if (!flag) {
      phenomenon = RequirementPhenomenonOperation.newPhenomenon(this.phenomenon_no++, name, type, from, to, false, reqno)
    }
    this.requirementPhenomenonList.push(phenomenon);

    return phenomenon;
  }
  //从interface,reference,和constraint的现象列表中找
  getPheByName(pheName) {
    for (let int of this.project.contextDiagram.interfaceList) {
      for (let phe of int.phenomenonList) {
        if (pheName == phe.name) return phe;
      }
    }
    for (let ref of this.project.problemDiagram.referenceList) {
      for (let phe of ref.phenomenonList) {
        if (pheName == phe.name) return phe;
      }
    }
    for (let ref of this.project.problemDiagram.constraintList) {
      for (let phe of ref.phenomenonList) {
        if (pheName == phe.name) return phe;
      }
    }
    return null;
  }
  //deletePhenomenon
  deletePhenomenon(name) {
    if (this.selectedType == "interface") {
      this.deletePhenomenonByName(
        name,
        this.phenomenonList
      );
      this.project.setDescription(this.interface);
      this.sendChangeShapeMessage(
        "change",
        "int",
        this.interface,
        this.interface
      );
    } else if (this.selectedType == "reference") {
      this.deletePhenomenonByName(
        name,
        this.requirementPhenomenonList
      );
      this.project.setDescription(this.reference);
      this.sendChangeShapeMessage(
        "change",
        "ref",
        this.reference,
        this.reference
      );
    } else if (this.selectedType == "constraint") {
      this.deletePhenomenonByName(
        name,
        this.requirementPhenomenonList
      );
      this.project.setDescription(this.constraint);
      this.sendChangeShapeMessage(
        "change",
        "con",
        this.constraint,
        this.constraint
      );
    }
  }
  deletePhenomenonByNo(phenomenonNo, phenomenonList): Phenomenon {
    let i = 0;
    for (let phenomenon of phenomenonList) {
      if (phenomenon.no == phenomenonNo) {
        phenomenonList.splice(i, 1);
        return phenomenon;
      }
      i += 1;
    }
  }
  deletePhenomenonByName(phenomenonName, phenomenonList): Phenomenon {
    let i = 0;
    for (let phenomenon of phenomenonList) {
      if (phenomenon.name == phenomenonName) {
        phenomenonList.splice(i, 1);
        return phenomenon;
      }
      i += 1;
    }
  }

  getAllPhenomenon() {
    let res = new Array();
    for (let interfacee of this.project.contextDiagram.interfaceList) {
      for (let phenomenon of interfacee.phenomenonList) {
        res.push(phenomenon);
      }
    }
    for (let reference of this.project.problemDiagram.referenceList) {
      for (let phenomenon of reference.phenomenonList) {
        res.push(phenomenon);
      }
    }
    for (let constraint of this.project.problemDiagram.constraintList) {
      for (let phenomenon of constraint.phenomenonList) {
        res.push(phenomenon);
      }
    }
    for (let i = 0; i < res.length; i++) {
      for (let j = res.length - 1; j > i; j--) {
        if (res[i].no == res[j].no && res[i].name == res[j].name) {
          res.splice(j, 1);
        }
      }
    }
    return res;
  }

  getAllRequirementPhenomenon() {
    let res = new Array();
    for (let reference of this.project.problemDiagram.referenceList) {
      for (let phenomenon of reference.phenomenonList) {
        res.push(phenomenon);
      }
    }
    for (let constraint of this.project.problemDiagram.constraintList) {
      for (let phenomenon of constraint.phenomenonList) {
        res.push(phenomenon);
      }
    }
    for (let i = 0; i < res.length; i++) {
      for (let j = res.length - 1; j > i; j--) {
        if (res[i].no == res[j].no && res[i].name == res[j].name) {
          res.splice(j, 1);
        }
      }
    }
    return res;
  }
  //========================================
  // 测试shortname是否包含文本端关键字
  isKeyWord(shortname: string) {
    for (let keyword of this.keyWords) {
      if (keyword == shortname) {
        console.log(shortname + " is a keyword");
        return true;
      }
    }
    return false;
  }
}