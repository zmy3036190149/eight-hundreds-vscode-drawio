import { Reference } from "../entity/Reference";
export class ReferenceOperation {

  static newShape(
    no,
    name,
    description,
    from,
    to,
    phenomenonList,
    x1,
    y1,
    x2,
    y2
  ) {
    let ref = new Reference();
    ref.no = no;
    ref.name = name;
    ref.description = description;
    ref.from = from;
    ref.to = to;
    if (phenomenonList) ref.phenomenonList = phenomenonList;
    else ref.phenomenonList = [];
    ref.x1 = x1;
    ref.y1 = y1;
    ref.x2 = x2;
    ref.y2 = y2;
    return ref;
  }

  static newShapeWithOld(old: Reference, description) {
    let ref = new Reference();
    ref.no = old.no;
    ref.name = old.name;
    ref.from = old.from;
    ref.to = old.to;
    ref.phenomenonList = old.phenomenonList;
    ref.description = description;
    ref.x1 = old.x1;
    ref.y1 = old.y1;
    ref.x2 = old.x2;
    ref.y2 = old.y2;
    return ref;
  }

  static copyShape(old: Reference) {
    let ref = new Reference();
    ref.no = old.no;
    ref.name = old.name;
    ref.from = old.from;
    ref.to = old.to;
    if (old.phenomenonList) ref.phenomenonList = old.phenomenonList;
    else ref.phenomenonList = [];
    ref.x1 = old.x1;
    ref.y1 = old.y1;
    ref.x2 = old.x2;
    ref.y2 = old.y2;
    Reference.setDescription1(ref);
    return ref;
  }
}