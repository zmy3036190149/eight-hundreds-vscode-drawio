import { ProblemDomain } from "../entity/ProblemDomain";
export class ProblemDomainOperation {
  static newShape(no, name, shortname, type, property, x, y, w, h) {
    let pd = new ProblemDomain();
    pd.no = no;
    pd.name = name;
    pd.shortname = shortname;
    pd.type = type;
    pd.property = property;
    pd.x = x;
    pd.y = y;
    pd.h = h;
    pd.w = w;
    return pd;
  }
  static newShapeWithOld(
    old: ProblemDomain,
    name,
    shortname,
    type,
    property
  ) {
    let pd = new ProblemDomain();
    pd.no = old.no;
    pd.x = old.x;
    pd.y = old.y;
    pd.h = old.h;
    pd.w = old.w;

    pd.name = name;
    pd.shortname = shortname;
    pd.type = type;
    pd.property = property;
    return pd;
  }
  static copyShape(old: ProblemDomain) {
    let pd = new ProblemDomain();
    pd.no = old.no;
    pd.x = old.x;
    pd.y = old.y;
    pd.h = old.h;
    pd.w = old.w;

    pd.name = old.name;
    pd.shortname = old.shortname;
    pd.type = old.type;
    pd.property = old.property;
    return pd;
  }
}