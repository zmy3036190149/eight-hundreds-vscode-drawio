import { Requirement } from "../entity/Requirement";
export class RequirementOperation {

  static newShape(no, name, x, y, h, w) {
    let req = new Requirement();
    req.no = no;
    req.name = name;
    req.shortname = this.getShortname(name);
    req.x = x;
    req.y = y;
    req.h = h;
    req.w = w;
    return req;
  }
  static newShapeWithOld(old: Requirement, name, shortname) {
    let req = new Requirement();
    req.no = old.no;
    req.name = name;
    req.shortname = shortname;
    req.x = old.x;
    req.y = old.y;
    req.h = old.h;
    req.w = old.w;
    return req;
  }
  static copyShape(old: Requirement) {
    let req = new Requirement();
    req.no = old.no;
    req.name = old.name;
    req.shortname = old.shortname ? old.shortname : this.getShortname(old.name);
    req.x = old.x;
    req.y = old.y;
    req.h = old.h;
    req.w = old.w;
    return req;
  }
  static getShortname(name: string) {
    return name.replace(/\s+/g, "");
  }
}