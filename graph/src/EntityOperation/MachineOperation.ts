import { Machine } from "../entity/Machine";
export class MachineOperation {

  static newShape(name, shortname, x, y, w, h) {
    let machine = new Machine();
    machine.name = name;
    machine.shortname = shortname;
    machine.x = x;
    machine.y = y;
    machine.h = h;
    machine.w = w;
    return machine;
  }
  static newShapeWithOld(old, name, shortname) {
    let machine = new Machine();
    machine.name = name;
    machine.shortname = shortname;
    machine.x = old.x;
    machine.y = old.y;
    machine.h = old.h;
    machine.w = old.w;
    return machine;
  }
  static copyShape(old) {
    if (old == null) return null;
    let machine = new Machine();
    machine.name = old.name;
    machine.shortname = old.shortname == "M" ? "M1" : old.shortname; //for text syn,M is a keyword
    machine.x = old.x;
    machine.y = old.y;
    machine.h = old.h;
    machine.w = old.w;
    return machine;
  }
  static copyShapeWithOldPos(old, new1) {
    let machine = this.copyShape(new1);
    machine.x = old.x;
    machine.y = old.y;
    machine.h = old.h;
    machine.w = old.w;
    return machine;
  }
}