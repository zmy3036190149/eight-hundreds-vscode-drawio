import { DiagramContentChangeEvent } from "./DiagramContentChangeEvent";

export class DiagramContentChangeEventFactory {
  public getShapeChangeEvent(
    shapeType,
    changeType,
    oldShape,
    newShape
  ): DiagramContentChangeEvent {
    let diagramContentChangeEvent: DiagramContentChangeEvent = {
      shapeType: shapeType,
      changeType: changeType,
      oldShape: oldShape,
      newShape: newShape,
      newProject: null
    };
    return diagramContentChangeEvent;
  }

  public getPhenomenonChangeEvent(
    changeType,
    line,
    oldPhenomenon,
    newPhenomenon
  ): DiagramContentChangeEvent {
    let diagramContentChangeEvent: DiagramContentChangeEvent = {
      shapeType: "phe",
      changeType: changeType,
      oldShape: null,
      newShape: null,
      newProject: null
    };
    return diagramContentChangeEvent;
  }
}
