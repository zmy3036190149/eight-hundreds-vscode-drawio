import { Project } from "../entity/Project";
export interface DiagramContentChangeEvent {
  shapeType: string; //"mac/pro/req/int/ref/con//phe//project"
  changeType: string; //"add/delete/change"

  //if shapeType in "mac/pro/req/int/ref/con"
  oldShape
  newShape


  newProject: Project;
}
