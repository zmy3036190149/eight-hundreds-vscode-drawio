export class Constraint {
  no: number;
  name: string;
  description: string;
  from: string;
  to: string;
  x1: number;
  y1: number;
  x2: number;
  y2: number;
  phenomenonList

  getNo() {
    return this.no;
  }
  setNo(no) {
    this.no = no;
  }

  getName() {
    return this.name;
  }
  setName(name) {
    this.name = name;
  }

  getDescription() {
    return this.description;
  }
  setDescription(description) {
    this.description = description;
  }

  getFrom() {
    return this.from;
  }
  setFrom(from) {
    this.from = from;
  }

  getTo() {
    return this.to;
  }
  setTo(to) {
    this.to = to;
  }

  getX1() {
    return this.x1;
  }
  setX1(x1) {
    this.x1 = x1;
  }

  getY1() {
    return this.y1;
  }
  setY1(y1) {
    this.y1 = y1;
  }

  getX2() {
    return this.x2;
  }
  setX2(x2) {
    this.x2 = x2;
  }

  getY2() {
    return this.y2;
  }
  setY2(y2) {
    this.y2 = y2;
  }
  getPhenomenonList() {
    return this.phenomenonList;
  }
  setPhenomenonList(phenomenonList) {
    this.phenomenonList = phenomenonList;
  }
  clearPhenomenonList() {
    this.phenomenonList.length = 0;
  }
  refreshPhenomenonList(oldShortName, newShortName) {
    for (let phenomenon of this.phenomenonList) {
      if (phenomenon.from == oldShortName) phenomenon.from = newShortName;
      else if (phenomenon.to == oldShortName) phenomenon.to = newShortName;
    }
  }
  static setDescription1(line) {
    let name = line.getName();
    let pheList = line.getPhenomenonList();
    //a:M!{on},P!{off}
    let s = "";
    s = s + name + ":";
    let s1 = "";
    let s2 = "";
    let desList = [];
    for (let phe of pheList) {
      let flag = false;
      for (let des of desList) {
        if (phe.from == des[0]) {
          des.push(phe.name);
          flag = true;
          break;
        }
      }
      if (!flag) {
        desList.push([phe.from, phe.name]);
      }
    }
    //console.log(desList);
    for (let des of desList) {
      s += des[0] + "!{";
      for (let item of des.slice(1)) {
        s += item + ",";
      }
      s = s.slice(0, -1);
      s += "},";
    }
    s = s.slice(0, -1);
    line.setDescription(s);
    return s;
  }
}
