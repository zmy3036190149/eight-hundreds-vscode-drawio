import { InterfaceOperation } from "../EntityOperation/InterfaceOperation";
import { MachineOperation } from "../EntityOperation/MachineOperation";
import { ProblemDomainOperation } from "../EntityOperation/ProblemDomainOperation";
import { Interface } from "./Interface";
import { Machine } from "./Machine";
import { ProblemDomain } from "./ProblemDomain";

export class ContextDiagram {
  title: string;
  machine: Machine;
  problemDomainList: ProblemDomain[];
  interfaceList: Interface[];
  static copyContextDiagram(old: ContextDiagram): ContextDiagram {
    let new1 = new ContextDiagram();
    new1.title = old.title;
    new1.machine = MachineOperation.copyShape(old.machine);
    new1.problemDomainList = new Array<ProblemDomain>();
    if (old.problemDomainList != null) {
      for (let oldPd of old.problemDomainList) {
        let pd = ProblemDomainOperation.copyShape(oldPd);
        new1.problemDomainList.push(pd);
      }
    }
    new1.interfaceList = new Array<Interface>();
    if (old.interfaceList != null) {
      for (let oldInt of old.interfaceList) {
        let int = InterfaceOperation.copyShape(oldInt);
        new1.interfaceList.push(int);
      }
    }
    return new1;
  }

  static copyContextDiagramWithOldPos(
    posContextDiagram: ContextDiagram,
    old: ContextDiagram
  ): ContextDiagram {
    let new1 = new ContextDiagram();
    new1.title = old.title;
    new1.machine = MachineOperation.copyShapeWithOldPos(
      posContextDiagram,
      old.machine
    );
    new1.problemDomainList = new Array<ProblemDomain>();
    if (old.problemDomainList != null) {
      for (let oldPd of old.problemDomainList) {
        let pd = ProblemDomainOperation.copyShape(oldPd);
        for (let pos of posContextDiagram.problemDomainList) {
          if (
            pos.getName() == pd.getName() ||
            pos.getShortname() == pd.getShortname()
          ) {
            pd.setH(pos.getH());
          }
        }
        new1.problemDomainList.push(pd);
      }
    }
    new1.interfaceList = new Array<Interface>();
    if (old.interfaceList != null) {
      for (let oldInt of old.interfaceList) {
        let int = InterfaceOperation.copyShape(oldInt);
        new1.interfaceList.push(int);
      }
    }
    return new1;
  }
}
