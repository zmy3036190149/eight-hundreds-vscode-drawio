var fs = require('fs')
var path = require("path");
var pathNames = ["drawioApp/js/entity", "drawioApp/js/EntityOperation",
    "drawioApp/js/Graph", "drawioApp/js/LSP"];

for (let pathName of pathNames)
    fs.readdir(pathName, function (err, files) {
        console.log(pathName)
        for (let i = 0; i < files.length; i++)
            rewrite(path.join(pathName, files[i]));
    });

rewrite = function (file) {
    fs.readFile(file, function (err, data) {
        var code = data.toString()
        // console.log("============================================")
        // console.log(code)
        var lines = code.split("\n")
        code = ""
        for (let i = 0; i < lines.length - 4; i++) {
            if (lines[i].indexOf("import") == 0 || lines[i].indexOf("var") == 0)
                continue;
            if (lines[i].indexOf("var window") != -1)
                continue;
            if (lines[i].indexOf("var vscode") != -1)
                continue;
            code += lines[i]
        }
        fs.writeFile(file, code, function (err) {
            if (err)
                console.log(err)
        })
    })
}

    // var file = '../drawioApp/js/entity/Constraint.js'

// cut the first line:
// console.log(lines.substring(lines.indexOf("\n") + 1));

// cut the last line:
// console.log(lines.substring(lines.lastIndexOf("\n") + 1, -1))