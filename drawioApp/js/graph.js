/*
 * $Id: Devel.js,v 1.27 2014/01/08 16:38:06 gaudenz Exp $
 * Copyright (c) 2006-2013, JGraph Ltd
 */
// This provides an indirection to make sure the mxClient.js
// loads before the dependent classes below are loaded. This
// is used for development mode where the JS is in separate
// files and the mxClient.js loads other files.

// // entity
var entityPath = "js/entity";
mxscript(entityPath + "/Constraint.js");
mxscript(entityPath + "/ContextDiagram.js");
mxscript(entityPath + "/Interface.js");
mxscript(entityPath + "/Machine.js");
mxscript(entityPath + "/Phenomenon.js");
mxscript(entityPath + "/problemDiagram.js");
mxscript(entityPath + "/Project.js");
mxscript(entityPath + "/Reference.js");
mxscript(entityPath + "/Requirement.js");
mxscript(entityPath + "/RequirementPhenomenon.js");

// EntityOperation
var entityOperationPath = "/js/EntityOperation";
mxscript(entityOperationPath + "/MachineOperation.js");
mxscript(entityOperationPath + "/RequirementOperation.js");
mxscript(entityOperationPath + "/ReferenceOperation.js");
mxscript(entityOperationPath + "/ConstraintOperation.js");
mxscript(entityOperationPath + "/InterfaceOperation.js");

// // Graph
var geGraphPath = "/js/Graph";
mxscript(geGraphPath + "/DrawGraphService.js");

