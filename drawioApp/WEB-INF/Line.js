define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Line = void 0;
    var Line = /** @class */ (function () {
        function Line() {
        }
        Line.prototype.getNo = function () { };
        Line.prototype.getName = function () {
            return "";
        };
        Line.prototype.getShortname = function () {
            throw new Error("Method not implemented.");
        };
        Line.prototype.getDescription = function () {
            return "";
        };
        Line.prototype.getFrom = function () {
            return "";
        };
        Line.prototype.getTo = function () {
            return "";
        };
        Line.prototype.getX1 = function () { };
        Line.prototype.getY1 = function () { };
        Line.prototype.getX2 = function () { };
        Line.prototype.getY2 = function () { };
        Line.prototype.getPhenomenonList = function () { };
        return Line;
    }());
    exports.Line = Line;
});
//# sourceMappingURL=Line.js.map