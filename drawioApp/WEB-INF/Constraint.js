define(["require", "exports", "./Line"], function (require, exports, Line_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Constraint = void 0;
    var Constraint = /** @class */ (function (_super) {
        __extends(Constraint, _super);
        function Constraint() {
            console.log("new Constraint");
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Constraint.prototype.getNo = function () {
            return this.no;
        };
        Constraint.prototype.setNo = function (no) {
            this.no = no;
        };
        Constraint.prototype.getName = function () {
            return this.name;
        };
        Constraint.prototype.setName = function (name) {
            this.name = name;
        };
        Constraint.prototype.getDescription = function () {
            return this.description;
        };
        Constraint.prototype.setDescription = function (description) {
            this.description = description;
        };
        Constraint.prototype.getFrom = function () {
            return this.from;
        };
        Constraint.prototype.setFrom = function (from) {
            this.from = from;
        };
        Constraint.prototype.getTo = function () {
            return this.to;
        };
        Constraint.prototype.setTo = function (to) {
            this.to = to;
        };
        Constraint.prototype.getX1 = function () {
            return this.x1;
        };
        Constraint.prototype.setX1 = function (x1) {
            this.x1 = x1;
        };
        Constraint.prototype.getY1 = function () {
            return this.y1;
        };
        Constraint.prototype.setY1 = function (y1) {
            this.y1 = y1;
        };
        Constraint.prototype.getX2 = function () {
            return this.x2;
        };
        Constraint.prototype.setX2 = function (x2) {
            this.x2 = x2;
        };
        Constraint.prototype.getY2 = function () {
            return this.y2;
        };
        Constraint.prototype.setY2 = function (y2) {
            this.y2 = y2;
        };
        Constraint.prototype.getPhenomenonList = function () {
            return this.phenomenonList;
        };
        Constraint.prototype.setPhenomenonList = function (phenomenonList) {
            this.phenomenonList = phenomenonList;
        };
        Constraint.prototype.clearPhenomenonList = function () {
            this.phenomenonList.length = 0;
        };
        Constraint.prototype.refreshPhenomenonList = function (oldShortName, newShortName) {
            for (var _i = 0, _a = this.phenomenonList; _i < _a.length; _i++) {
                var phenomenon = _a[_i];
                if (phenomenon.from == oldShortName)
                    phenomenon.from = newShortName;
                else if (phenomenon.to == oldShortName)
                    phenomenon.to = newShortName;
            }
        };
        Constraint.setDescription1 = function (line) {
            var name = line.getName();
            var pheList = line.getPhenomenonList();
            //a:M!{on},P!{off}
            var s = "";
            s = s + name + ":";
            var s1 = "";
            var s2 = "";
            var desList = [];
            for (var _i = 0, pheList_1 = pheList; _i < pheList_1.length; _i++) {
                var phe = pheList_1[_i];
                var flag = false;
                for (var _a = 0, desList_1 = desList; _a < desList_1.length; _a++) {
                    var des = desList_1[_a];
                    if (phe.from == des[0]) {
                        des.push(phe.name);
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    desList.push([phe.from, phe.name]);
                }
            }
            //console.log(desList);
            for (var _b = 0, desList_2 = desList; _b < desList_2.length; _b++) {
                var des = desList_2[_b];
                s += des[0] + "!{";
                for (var _c = 0, _d = des.slice(1); _c < _d.length; _c++) {
                    var item = _d[_c];
                    s += item + ",";
                }
                s = s.slice(0, -1);
                s += "},";
            }
            s = s.slice(0, -1);
            line.setDescription(s);
            return s;
        };
        return Constraint;
    }(Line_1.Line));
    exports.Constraint = Constraint;
});
//# sourceMappingURL=Constraint.js.map