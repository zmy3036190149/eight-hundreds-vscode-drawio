const path = require('path');
module.exports = {
  entry: { app: './graph/src/Graph/DrawGraphService.ts' },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'graph')
  },
  mode: 'development' // mode 用来指定构建模式 
} 